package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.Blob;
import com.mysql.cj.x.protobuf.MysqlxConnection.Close;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class UserView extends JFrame implements ActionListener{
	JPanel panel;
	JComboBox comCat;
	JComboBox comProduct;
	JLabel lblName;
	JLabel lblRs;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private static ImageIcon itemImage;
	private static JLabel imgLabel;
	private static JLabel itemTxt;
	JButton btnSearch;
	JPanel panel_2;
	private JLabel lblimg;
	JButton btnbuy;
	JLabel lblamountDisplay;
	JLabel lblamount;
	JLabel lblbrandName;
	JLabel lblitemName ;
	private JLabel lblavailable;
	private JLabel lblquanAvail;
	JSpinner spinner;
	JButton btnCatogory;
	JButton btnproductAdd;
	JButton btnitemAdd;
	JButton btnviewOrder;
	private JButton btnsignout;
	public UserView() {
		getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(Color.GREEN);
		panel.setForeground(Color.BLACK);
		panel.setBounds(0, 0, 989, 48);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lbltitle = new JLabel("Welcome...! QUICK SHOP");
		lbltitle.setBounds(10, 15, 198, 21);
		panel.add(lbltitle);
		lbltitle.setFont(new Font("Times New Roman", Font.ITALIC, 18));
		
		btnsignout = new JButton("SignOut");
		btnsignout.setBounds(890, 11, 89, 26);
		panel.add(btnsignout);
		btnsignout.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		btnCatogory = new JButton("Add Category");
		btnCatogory.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnCatogory.setBounds(404, 16, 127, 23);
		panel.add(btnCatogory);
		
		btnproductAdd = new JButton("Add Product");
		btnproductAdd.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnproductAdd.setBounds(541, 16, 112, 23);
		panel.add(btnproductAdd);
		
		btnitemAdd = new JButton("Add Item");
		btnitemAdd.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnitemAdd.setBounds(666, 16, 89, 23);
		panel.add(btnitemAdd);
		
		btnviewOrder = new JButton("View Orders");
		btnviewOrder.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnviewOrder.setBounds(765, 16, 115, 23);
		panel.add(btnviewOrder);
		btnsignout.addActionListener(this);
		
		JLabel lblCat = new JLabel("Category");
		lblCat.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblCat.setBounds(10, 59, 73, 25);
		getContentPane().add(lblCat);
		
		JLabel lblproduct = new JLabel("Product");
		lblproduct.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblproduct.setBounds(157, 59, 73, 19);
		getContentPane().add(lblproduct);
		
		comCat = new JComboBox();
		comCat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		comCat.setBounds(10, 82, 126, 22);
		getContentPane().add(comCat);
		
		//comCat.setModel(new DefaultComboBoxModel(new String[] {"ALL"}));
		
		comProduct = new JComboBox();
		comProduct.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		comProduct.setBounds(157, 82, 126, 22);
		getContentPane().add(comProduct);
		
		lblName = new JLabel("");
		lblName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblName.setBounds(196, 132, 73, 25);
		getContentPane().add(lblName);
		
		lblRs = new JLabel("");
		lblRs.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblRs.setBounds(271, 182, 91, 25);
		getContentPane().add(lblRs);
		comboSetText(comCat);
		comboSetText(comProduct);
		
		panel_1 = new JPanel();
		panel_1.setBounds(10, 115, 564, 422);
		getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 10, 10));
		
		btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnSearch.setBounds(303, 81, 89, 25);
		getContentPane().add(btnSearch);
		
		panel_2 = new JPanel();
		panel_2.setBounds(621, 115, 319, 422);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		lblimg = new JLabel("");
		lblimg.setBounds(95, 11, 129, 101);
		panel_2.add(lblimg);
	
		lblitemName = new JLabel("");
		lblitemName.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblitemName.setBounds(95, 116, 116, 28);
		panel_2.add(lblitemName);
		
		JLabel lblBrand = new JLabel("Brand:");
		lblBrand.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblBrand.setBounds(56, 163, 57, 28);
		panel_2.add(lblBrand);
		
		lblbrandName = new JLabel("brandName");
		lblbrandName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblbrandName.setBounds(139, 163, 85, 28);
		panel_2.add(lblbrandName);
		
		JLabel lblprice = new JLabel("Price(Rs.):");
		lblprice.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblprice.setBounds(56, 206, 77, 21);
		panel_2.add(lblprice);
		
		lblamount = new JLabel("amount");
		lblamount.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblamount.setBounds(139, 202, 103, 25);
		panel_2.add(lblamount);
		
		JLabel lblquan = new JLabel("Quantity:");
		lblquan.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblquan.setBounds(56, 238, 77, 28);
		panel_2.add(lblquan);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(1, 1, null, 1));
		spinner.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		spinner.setBounds(145, 243, 48, 20);
		panel_2.add(spinner);
		
		JLabel lbltotal = new JLabel("Total Amount(Rs.):");
		lbltotal.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbltotal.setBounds(56, 280, 127, 28);
		panel_2.add(lbltotal);
		
		lblamountDisplay = new JLabel();
		lblamountDisplay.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblamountDisplay.setBounds(193, 284, 93, 24);
		panel_2.add(lblamountDisplay);
		
		btnbuy = new JButton("Buy Now");
		btnbuy.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnbuy.setBounds(95, 349, 89, 23);
		panel_2.add(btnbuy);
		
		lblavailable = new JLabel();
		lblavailable.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblavailable.setBounds(258, 245, 30, 21);
		panel_2.add(lblavailable);
		
		lblquanAvail = new JLabel("Available");
		lblquanAvail.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblquanAvail.setBounds(203, 242, 57, 21);
		panel_2.add(lblquanAvail);
		
		JButton btnImg = new JButton();
	    btnImg.setLayout(new BorderLayout());
	    imgLabel = new JLabel();
	    itemTxt = new JLabel();
	    itemTxt.setBackground(new Color(215, 219, 226));
	    itemTxt.setLayout(new BorderLayout(0,0));
	    btnImg.add(imgLabel, BorderLayout.NORTH);
	    btnImg.add(itemTxt, BorderLayout.SOUTH);
	    // Add Button to Grid
		loadCategories();
		comCat.addActionListener(this);
		loadProducts();
		loadItems();
		
		btnSearch.addActionListener(this);
		btnbuy.addActionListener(this);
		btnCatogory.addActionListener(this);
		
			btnproductAdd.addActionListener(this);
			btnitemAdd.addActionListener(this);
			btnviewOrder.addActionListener(this);
		
	}
	public void isAdminUser(String UserId) {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select Administration from Users where userId="+UserId;
	    System.out.println(sql1);
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load Users !!!!");
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql1);
		    int AdminId = 0;
            while (rs.next()) 
            {
                AdminId  = rs.getInt("Administration");
            }
            if(AdminId != 1) {
            	btnproductAdd.setVisible(false);
    			btnitemAdd.setVisible(false);
    			btnCatogory.setVisible(false);
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	
	}
	public void comboSetText(JComboBox combo) {
		combo.setRenderer((ListCellRenderer<? super Object[]>) new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                Object[] itemData = (Object[])value;
                setText((String) itemData[1]);
                return this;
            }
        } );
	}
	public void loadCategories() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from Categories";
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load categories !!!!");
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql1);
		    comCat.addItem(new Object[] {-1, "All Categories"});
            while (rs.next()) {
                String name  = rs.getString("CategoryName");;
                int id  = rs.getInt("Category_Id");
                comCat.addItem(new Object[] {id, name});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	public void loadProducts() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from products";
		Object selected_category = comCat.getSelectedItem();
		int categoryId = (int) Array.get(selected_category, 0);
		if(categoryId != -1)
		{
			sql1 = sql1.concat(" where Category_Id="+categoryId);
		}
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load products!!!!");
			st = con.createStatement();
		    ResultSet rs = st.executeQuery(sql1);
		    comProduct.removeAllItems();
		    comProduct.addItem(new Object[] {-1, "All Products"});
            while (rs.next()) {
                String name  = rs.getString("ProductName");;
                int id  = rs.getInt("ProductId");
                comProduct.addItem(new Object[] {id, name});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	    
	}
	public void loadItems() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from Items";
	    Object selected_product = comProduct.getSelectedItem();
		int productId = (int) Array.get(selected_product, 0);
		if(productId != -1)
		{
			sql1 = sql1.concat(" where ProductId="+productId);
		}
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load categories !!!!");
			st = con.createStatement();
		    ResultSet rs = st.executeQuery(sql1);
		    panel_1.removeAll();
		    panel_1.revalidate();
		    panel_1.repaint();
		    while (rs.next()) {
                String name  = rs.getString("name");;
                String id  = rs.getString("Id");
                byte[] img  = rs.getBytes("image");
                String price = rs.getString("price");
                String brand = rs.getString("brand");
                String prodId = rs.getString("ProductId");
                ImageIcon image = new ImageIcon(img);
                Image im = image.getImage();
                Image myImage =im.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
                ImageIcon newImage= new ImageIcon(myImage);

                JButton itemContainer = new JButton();
                itemContainer.setLayout(new GridLayout());
                itemContainer.setName(id);
                
                JLabel itemImage = new JLabel();
                JLabel itemText = new JLabel();
          
                itemText.setBackground(new Color(215, 219, 226));
                itemText.setText("<html>"+name + "<br>Brand : " + brand +"<br>Price (Rs.): "+price+"</html>");        
                itemImage.setIcon(newImage);
               
                //itemContainer.add(getid,BorderLayout.SOUTH);
                itemContainer.add(itemImage, BorderLayout.WEST);
                itemContainer.add(itemText, BorderLayout.EAST);
               // Add Button to Grid
        	    panel_1.add(itemContainer);
        	    panel_1.getComponent(0);
        	    
        	    itemContainer.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						String MySQLURL = "jdbc:mysql://localhost:3306/project";
					    String databseUserName = "root";
					    String databasePassword = "Alliswell@43";
					    Connection con = null;
					    Statement st;
					    String sql1 = "select * from Items where id="+id;
					    try {
							con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
							if(con != null)
								System.out.println("Database connection is successful for load Item Details!");
							st = con.createStatement();
						    ResultSet rs = st.executeQuery(sql1);
						   /* panel_2.removeAll();
						    panel_2.revalidate();
						    panel_2.repaint();*/
						    while (rs.next()) {
				                String name  = rs.getString("name");;
				                String id  = rs.getString("Id");
				                byte[] img  = rs.getBytes("image");
				                String price = rs.getString("price");
				                String brand = rs.getString("brand");
				                String quantity = rs.getString("quantity");
				                ImageIcon image = new ImageIcon(img);
				                Image im = image.getImage();
				                Image myImage =im.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
				                ImageIcon newImage= new ImageIcon(myImage);
				               
				                lblimg.setIcon(newImage);
				                lblitemName.setText(name);
				                lblbrandName.setText(brand);
				                lblamount.setText(price);
				                lblavailable.setText(quantity);
				                
				                spinner.setModel(new SpinnerNumberModel(1, 1, Integer.parseInt(quantity), 1));
				                spinner.addChangeListener(new ChangeListener() {
									@Override
									public void stateChanged(ChangeEvent e) {
										// TODO Auto-generated method stub
							            
										Float total = Float.parseFloat(spinner.getValue().toString())*Float.parseFloat(price);
						                lblamountDisplay.setText(Float.toString(total));
									}
								});
				                Float total = 1*Float.parseFloat(price);
				                lblamountDisplay.setText(Float.toString(total));
				                
				                panel_2.add(lblimg);
				                panel_2.add(lblbrandName);
				                panel_2.add(lblamount);
				                btnbuy.setName(id);
						    }
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==comCat)
		{
			loadProducts();
		}
		else if(e.getSource() == btnSearch)
		{
			loadItems();
		}
		else if(e.getSource() == btnbuy)
		{
			String quantity = spinner.getValue().toString();
			String totalAmount = lblamountDisplay.getText();
			String itemId = btnbuy.getName();
			String userId = getName();
			UserViewCode uc = new UserViewCode();
			uc.insertOrder(quantity,totalAmount,itemId,userId);
			JOptionPane.showMessageDialog(this, "Your Order is Placed Successfully!! \n              Thank You...");
		}
		else if(e.getSource() == btnsignout)
		{
			LoginPage frame = new LoginPage();
			frame.setTitle("LoginPage");
			frame.setVisible(true);
			frame.setBounds(10, 10, 500, 400);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		else if(e.getSource() == btnCatogory)
		{
			 CategoryPage frame = new CategoryPage();
			 frame.setTitle("Add Category");
			 frame.setVisible(true);
			 frame.setBounds(10, 10, 500, 600);
			 //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		else if(e.getSource() == btnproductAdd)
		{
			   Products frame = new Products();
			   frame.setTitle("Add Product");
			   frame.setVisible(true);
			   frame.setBounds(10, 10, 500, 600);
			   //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		else if(e.getSource() == btnitemAdd)
		{
			Items  frame = new Items ();
			frame.setTitle("Add Item");
			frame.setVisible(true);
			frame.setBounds(10, 10, 500, 600);
			//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		else if(e.getSource() == btnviewOrder)
		{
			String userId = getName();
			String itemId = btnbuy.getName();
			OrderDetails frame = new OrderDetails();
			 frame.setTitle("View Items");
			 frame.setVisible(true);
			 frame.setBounds(10, 10, 500, 600);
			 //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 frame.setName(userId);
			 frame.loadOrderDetails();
						
		}
	}
}
