package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Window;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import java.awt.Color;

public class LoginPage extends JFrame implements ActionListener{
	private JTextField textFieldUN;
	JPasswordField passwordField;
	JButton btnreg = new JButton("Signup");
	JButton btnlogin = new JButton("Log in");
	static LoginPage frame = new LoginPage();
	JCheckBox checkpass = new JCheckBox("Show Password");
	JButton btnAdmin; 
	public LoginPage() {
		getContentPane().setLayout(null);
		
		JLabel lblwelcome = new JLabel("LOGIN HERE....!");
		lblwelcome.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblwelcome.setBounds(204, 0, 114, 30);
		getContentPane().add(lblwelcome);
		
		btnreg.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnreg.setBounds(394, 252, 89, 23);
		getContentPane().add(btnreg);
		
		btnlogin.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnlogin.setBounds(284, 212, 89, 23);
		getContentPane().add(btnlogin);
		
		JLabel lblusername = new JLabel("User Name");
		lblusername.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblusername.setBounds(247, 101, 78, 23);
		getContentPane().add(lblusername);
		
		textFieldUN = new JTextField();
		textFieldUN.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		textFieldUN.setBounds(247, 121, 146, 20);
		getContentPane().add(textFieldUN);
		textFieldUN.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		passwordField.setBounds(247, 171, 146, 20);
		getContentPane().add(passwordField);
		
		JLabel lblreg = new JLabel("New User? Register Here..");
		lblreg.setBounds(247, 257, 154, 14);
		getContentPane().add(lblreg);
		
		checkpass.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		checkpass.setBounds(399, 169, 124, 27);
		getContentPane().add(checkpass);
		
		btnAdmin = new JButton("Administrator");
		btnAdmin.setBackground(Color.WHITE);
		btnAdmin.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAdmin.setBounds(46, 235, 124, 23);
		getContentPane().add(btnAdmin);
		
		JLabel lblNewLabel = new JLabel("Click here for ADMIN login");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setBounds(46, 210, 126, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblPassword.setBounds(247, 152, 78, 23);
		getContentPane().add(lblPassword);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBackground(Color.WHITE);
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\ELCOT\\Downloads\\Admin.jpg"));
		lblNewLabel_1.setBounds(10, 70, 201, 136);
		getContentPane().add(lblNewLabel_1);
		
		btnreg.addActionListener(this);
		btnlogin.addActionListener(this);
		checkpass.addActionListener(this);
		btnAdmin.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String username=null;
		String password=null;
		LoginPageCode lp = new LoginPageCode();
		if (e.getSource() == btnlogin)
		{
			username = textFieldUN.getText();
			password = passwordField.getText();
			String userId =  lp.loginValidation(username,password);
			if(userId != "") {
				JOptionPane.showMessageDialog(this, "Login Successfully ");
				UserView frame = new UserView();
				 frame.setTitle("View Items");
				   frame.setVisible(true);
				   frame.setName(userId);
				   frame.setBounds(10, 10, 500, 600);
				   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                   frame.isAdminUser(userId);
			}else {
				JOptionPane.showMessageDialog(this, "User credentials are not valid");
			}
		}
		else if(e.getSource() == checkpass)
		{
			if(checkpass.isSelected())
			{
				passwordField.setEchoChar((char)0);
			}
			else
			{
				passwordField.setEchoChar('*');
			}
		}
		else if (e.getSource() == btnreg)
		{
			//frame.setVisible(false);
			RegisterPage regframe = new RegisterPage();
			regframe.setTitle("RegisterPage");
			regframe.setVisible(true);
			regframe.setBounds(10, 10, 370, 600);
			regframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		else if(e.getSource() == btnAdmin)
		{
			textFieldUN.setText("Admin");
			passwordField.setText("Admin");
			btnlogin.doClick();
			 /*CategoryPage frame = new CategoryPage();
			 frame.setTitle("View Items");
			 frame.setVisible(true);
			 frame.setBounds(10, 10, 500, 600);
			 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 */
		}
		
	}
	public static void main(String[] args) {
		frame.setTitle("LoginPage");
		frame.setVisible(true);
		frame.setBounds(10, 10, 500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
}
