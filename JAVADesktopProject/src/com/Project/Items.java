package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Items extends JFrame implements ActionListener {
	private JTextField textFielditem;
	private JTextField textFieldBrand;
	private JTextField textFieldPrice;
	private JTextField textFieldQuan;
	JComboBox<Object[]> comboBox = new JComboBox();
	JComboBox<Object[]> comboBox_1 = new JComboBox();
	JButton btnAddItem;
	JButton btnBrowse;
	JLabel lblNewLabel; 
	String filePath;
	public Items() {
		getContentPane().setLayout(null);
		
		JLabel lblItem = new JLabel("Add Items");
		lblItem.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblItem.setBounds(163, 11, 102, 25);
		getContentPane().add(lblItem);
		
		JLabel lblCat = new JLabel("Category");
		lblCat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblCat.setBounds(47, 54, 72, 25);
		getContentPane().add(lblCat);
		
		comboBox.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		comboBox.setBounds(120, 54, 105, 25);
		getContentPane().add(comboBox);
		
		comboBox.setRenderer((ListCellRenderer<? super Object[]>) new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                Object[] itemData = (Object[])value;
                setText((String) itemData[1]);
                return this;
            }
        } );
		
		JLabel lblProduct = new JLabel("Product");
		lblProduct.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblProduct.setBounds(254, 54, 72, 25);
		getContentPane().add(lblProduct);
		
		
		comboBox_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		comboBox_1.setBounds(323, 55, 105, 23);
		getContentPane().add(comboBox_1);
		
		comboBox_1.setRenderer((ListCellRenderer<? super Object[]>) new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                Object[] itemData = (Object[])value;
                setText((String) itemData[1]);
                return this;
            }
        } );
		
		JLabel lblAddItem = new JLabel("ItemName");
		lblAddItem.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblAddItem.setBounds(47, 106, 93, 25);
		getContentPane().add(lblAddItem);
		
		textFielditem = new JTextField();
		textFielditem.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		textFielditem.setBounds(144, 107, 121, 25);
		getContentPane().add(textFielditem);
		textFielditem.setColumns(10);
		
		JLabel lblBrand = new JLabel("Brand");
		lblBrand.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblBrand.setBounds(47, 151, 72, 23);
		getContentPane().add(lblBrand);
		
		textFieldBrand = new JTextField();
		textFieldBrand.setBounds(144, 151, 121, 25);
		getContentPane().add(textFieldBrand);
		textFieldBrand.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblPrice.setBounds(47, 201, 93, 23);
		getContentPane().add(lblPrice);
		
		textFieldPrice = new JTextField();
		textFieldPrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		textFieldPrice.setBounds(144, 202, 121, 23);
		getContentPane().add(textFieldPrice);
		textFieldPrice.setColumns(10);
		
		JLabel lblQuan = new JLabel("Quantity");
		lblQuan.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblQuan.setBounds(47, 248, 72, 25);
		getContentPane().add(lblQuan);
		
		textFieldQuan = new JTextField();
		textFieldQuan.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		textFieldQuan.setBounds(144, 251, 121, 22);
		getContentPane().add(textFieldQuan);
		textFieldQuan.setColumns(10);
		
		JLabel lblimg = new JLabel("Image");
		lblimg.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblimg.setBounds(47, 302, 72, 25);
		getContentPane().add(lblimg);
		
		btnAddItem = new JButton("Save");
		btnAddItem.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAddItem.setBounds(163, 421, 89, 23);
		getContentPane().add(btnAddItem);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnBrowse.setBounds(144, 304, 89, 23);
		getContentPane().add(btnBrowse);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(144, 338, 121, 72);
		getContentPane().add(lblNewLabel);
		
		btnBrowse.addActionListener(new ActionListener(){
	        @Override
	     public void actionPerformed(ActionEvent e){
	         JFileChooser fileChooser = new JFileChooser();
	         fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
	         FileNameExtensionFilter filter = new FileNameExtensionFilter("*.IMAGE", "jpg","gif","png");
	         fileChooser.addChoosableFileFilter(filter);
	         int result = fileChooser.showSaveDialog(null);
	         if(result == JFileChooser.APPROVE_OPTION){
	             File selectedFile = fileChooser.getSelectedFile();
	             filePath = selectedFile.getAbsolutePath();
	             lblNewLabel.setIcon(ResizeImage(filePath));
	             
	              }
	         else if(result == JFileChooser.CANCEL_OPTION){
	             System.out.println("No Data");
	         }
	     }
	    });
		loadCategories();
		loadProducts();
		
		btnAddItem.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnAddItem)
		{
			String item = textFielditem.getText();
			String brand = textFieldBrand.getText();
			float price =Float.parseFloat(textFieldPrice.getText());
			int quantity =Integer.parseInt(textFieldQuan.getText());
			Object selected_category = comboBox.getSelectedItem();
			Object selected_product = comboBox_1.getSelectedItem();
			//int category_id = (int) Array.get(selected_category,0);
			int product_id = (int) Array.get(selected_product,0);
			InputStream imgstream = null;
			try {
				imgstream = new FileInputStream(new File(filePath));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			ItemsCode ic = new ItemsCode();
			ic.insertItems(item,brand,price,quantity,imgstream,product_id);
		}
	}
	public void loadCategories() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from Categories";
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load categories !!!!");
			st = con.createStatement();
		   
		   
		    ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                String name  = rs.getString("CategoryName");;
                int id  = rs.getInt("Category_Id");
                comboBox.addItem(new Object[] {id, name});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	public void loadProducts() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from products";
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful for load products!!!!");
			st = con.createStatement();
		   
		   
		    ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                String name  = rs.getString("ProductName");;
                int id  = rs.getInt("ProductId");
                comboBox_1.addItem(new Object[] {id, name});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	    
	}
	public ImageIcon ResizeImage(String imgPath){
        ImageIcon MyImage = new ImageIcon(imgPath);
        Image img = MyImage.getImage();
        Image newImage = img.getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(),Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImage);
        return image;
    }
	/*public static void main(String[] args)
	{
		Items  frame = new Items ();
		frame.setTitle("View Items");
		frame.setVisible(true);
		frame.setBounds(10, 10, 500, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}*/
}
