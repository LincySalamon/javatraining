package com.Project;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

public class OrderDetails extends JFrame {
	private JTable table_1;
	public OrderDetails() {
		getContentPane().setLayout(new BorderLayout(0, 0));
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Item Name", "Quantity", "Total Amount"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		getContentPane().add(table_1, BorderLayout.WEST);
	}
	
	/*public void OrderDetails() {
		getContentPane().setLayout(new BorderLayout(0, 0));
		table = new JTable();
		//scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Item Name", "Quantity", "Total Amount"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		
		getContentPane().add(table);


	}
	public static void main(String[] args) {
		OrderDetails frame = new OrderDetails();
		 frame.setTitle("View Items");
		 frame.setVisible(true);
		 frame.setBounds(10, 10, 500, 600);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// OrderDetails vo = new OrderDetails();
	}*/
	
	public void loadOrderDetails() {
		String UserId = getName();
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    ResultSet rs;
	    DefaultTableModel model =(DefaultTableModel) table_1.getModel();
	    try {
	    	con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
	    	if(con != null)
				System.out.println("Database connection is successful for load Item Details!");
	    	st = con.createStatement();
	    	String sql = "select i.name,o.quantity,o.total_amount from orders o inner join items i "
		    		+ "where o.itemId = i.id and o.userId="+UserId;
	    	rs = st.executeQuery(sql);
	    	System.out.println(sql);
	    	while(rs.next())
	    	{
	    		String name = rs.getString("name");
	    		String quantity = rs.getString("quantity");
	    		String total = rs.getString("total_amount");
	    		model.addRow(new String[] {name,quantity,total});
	    	}
	    	//model.addRow(new String[] {"ss","sss","aaa"});
	    	}catch(Exception e) {
	    		
	    	  e.printStackTrace();
	        }
	}
}
