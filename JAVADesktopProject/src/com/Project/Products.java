package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;

public class Products extends JFrame implements ActionListener{
	private JTextField textFieldProducts;
	JButton btnAddProduct = new JButton("Add Products");
	JComboBox<Object[]> comboBox = new JComboBox();
	JButton btnAdditem;
	public Products() {
		getContentPane().setLayout(null);
		
		JLabel lblAddpro = new JLabel("Add Product");
		lblAddpro.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblAddpro.setBounds(175, 23, 123, 25);
		getContentPane().add(lblAddpro);
		
		JLabel lblCat = new JLabel("Choose Category");
		lblCat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblCat.setBounds(57, 68, 112, 25);
		getContentPane().add(lblCat);
		
		//comboBox.setModel(new DefaultComboBoxModel(new String[] {"Mobiles", "Laptops"}));
		
		comboBox.setBounds(185, 66, 145, 31);
		getContentPane().add(comboBox);
		
		JLabel lblAdd = new JLabel("Products");
		lblAdd.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblAdd.setBounds(57, 145, 112, 31);
		getContentPane().add(lblAdd);
		
		textFieldProducts = new JTextField();
		textFieldProducts.setBounds(185, 151, 153, 25);
		getContentPane().add(textFieldProducts);
		textFieldProducts.setColumns(10);
		
		btnAddProduct.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAddProduct.setBounds(57, 220, 145, 31);
		getContentPane().add(btnAddProduct);
		
		
		btnAdditem = new JButton("Save and add Items");
		btnAdditem.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAdditem.setBounds(249, 220, 130, 31);
		getContentPane().add(btnAdditem);
		
		btnAddProduct.addActionListener(this);
		btnAdditem.addActionListener(this);
		comboBox.addActionListener(this);
		
		comboBox.setRenderer((ListCellRenderer<? super Object[]>) new DefaultListCellRenderer() {
	            @Override
	            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
	                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	                Object[] itemData = (Object[])value;
	                setText((String) itemData[1]);
	                return this;
	            }
	        } );
		
		loadCategories();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnAddProduct)
		{
			String product_name = textFieldProducts.getText();
			Object selected_category = comboBox.getSelectedItem();
			int category_id = (int) Array.get(selected_category,0);
			ProductsCode pc = new ProductsCode();
			pc.insertProducts(product_name,category_id);
		}
		else if(e.getSource() == comboBox)
		{
			//s = (Object)comboBox.getSelectedItem();
			//id = (int) Array.get(s,0);
			//System.out.print(id);
			//CategoryName = comboBox.add(Products, comboBox);
		}
		else if(e.getSource() == btnAdditem)
		{
			Items  frame = new Items ();
			frame.setTitle("View Items");
			frame.setVisible(true);
			frame.setBounds(10, 10, 500, 600);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		
	}
	public void loadCategories() {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
	    String databseUserName = "root";
	    String databasePassword = "Alliswell@43";
	    Connection con = null;
	    Statement st;
	    String sql1 = "select * from Categories";
	    try {
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful !!!!");
			st = con.createStatement();
		   
		   
		    ResultSet rs = st.executeQuery(sql1);

            while (rs.next()) {
                String name  = rs.getString("CategoryName");;
                int id  = rs.getInt("Category_Id");
                comboBox.addItem(new Object[] {id, name});
            }
		   }
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	/*public static void main(String[] args) {
		Products frame = new Products();
		 frame.setTitle("View Items");
		   frame.setVisible(true);
		   frame.setBounds(10, 10, 500, 600);
		   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}*/
}
