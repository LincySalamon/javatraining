package com.Project;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class ItemsCode {
	public int insertItems(String item,String brand,float price,int quantity,InputStream imgstream,int product_id) {
		String MySQLURL = "jdbc:mysql://localhost:3306/project";
		String databseUserName = "root";
		String databasePassword = "Alliswell@43";
		Connection con = null;
		try{
			con = DriverManager.getConnection(MySQLURL, databseUserName, databasePassword);
			if (con != null) 
				System.out.println("Database connection is successful !!!!");
			String sql = "insert into items(name,brand,price,quantity,image,productId) values(?,?,?,?,?,?)";
	        System.out.println(sql);
	        PreparedStatement ps = con.prepareStatement(sql);
	        ps.setString(1, item);
	        ps.setString(2, brand);
	        ps.setFloat(3, price);
	        ps.setInt(4, quantity);
	        ps.setBlob(5, imgstream);
	        ps.setInt(6, product_id);
	        ps.executeUpdate();
	        JOptionPane.showMessageDialog(null, "Item Added");
	       }catch(Exception ex){
	           ex.printStackTrace();
	       }
	    return 0;
	}
}
