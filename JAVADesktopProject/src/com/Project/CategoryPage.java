package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JMenu;
import javax.swing.JButton;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollBar;

public class CategoryPage extends JFrame implements ActionListener{
	private JTextField textField;
	JButton btnAddCat = new JButton("Save Category");
	JButton btnproducts = new JButton("Add Products");
	
	public CategoryPage() {
		getContentPane().setLayout(null);
		
		JLabel lblcat = new JLabel("Add Category");
		lblcat.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblcat.setBounds(172, 25, 111, 45);
		getContentPane().add(lblcat);
		
		JLabel lblCN = new JLabel("Category");
		lblCN.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblCN.setBounds(53, 93, 84, 31);
		getContentPane().add(lblCN);
		
		textField = new JTextField();
		textField.setBounds(172, 99, 135, 25);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		btnAddCat.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnAddCat.setBounds(33, 165, 220, 31);
		getContentPane().add(btnAddCat);
		
		btnproducts.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnproducts.setBounds(283, 170, 142, 25);
		getContentPane().add(btnproducts);
		
		btnAddCat.addActionListener(this);
		btnproducts.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String category = null;
		CategoryPageCode cp = new CategoryPageCode();
		if(e.getSource() == btnAddCat)
		{
			category = textField.getText();
			cp.insertCategory(category);
			//JOptionPane.showMessageDialog(this, "Category Added Successfully ");
		}

		if(e.getSource() == btnproducts )
		{
			Products frame = new Products();
			 frame.setTitle("View Items");
			   frame.setVisible(true);
			   frame.setBounds(10, 10, 500, 600);
			   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}
	/*public static void  main(String[] args) {
		CategoryPage frame = new CategoryPage();
		 frame.setTitle("View Items");
		 frame.setVisible(true);
		 frame.setBounds(10, 10, 500, 600);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 
	}*/
}
