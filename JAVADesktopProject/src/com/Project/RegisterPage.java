package com.Project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;


import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class RegisterPage extends JFrame implements ActionListener {
	private JTextField textFN;
	private JTextField textLN;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	JTextArea textArea = new JTextArea();
	JButton btnregister = new JButton("Register");
	private JTextField textFieldUN;
	private JTextField textFieldMN;
	
	public RegisterPage() {
		getContentPane().setLayout(null);
		
		JLabel lblregHere = new JLabel("Registered Here...");
		lblregHere.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblregHere.setBounds(120, 11, 143, 26);
		getContentPane().add(lblregHere);
		
		JLabel lblFN = new JLabel("First Name");
		lblFN.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblFN.setBounds(53, 48, 96, 26);
		getContentPane().add(lblFN);
		
		textFN = new JTextField();
		textFN.setBounds(189, 48, 150, 26);
		getContentPane().add(textFN);
		textFN.setColumns(10);
		
		JLabel lblLN = new JLabel("Last Name");
		lblLN.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblLN.setBounds(53, 86, 96, 17);
		getContentPane().add(lblLN);
		
		textLN = new JTextField();
		textLN.setFont(new Font("Sitka Text", Font.PLAIN, 14));
		textLN.setBounds(189, 85, 150, 25);
		getContentPane().add(textLN);
		textLN.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		passwordField.setBounds(189, 213, 150, 26);
		getContentPane().add(passwordField);
		
		JLabel lblPW = new JLabel("Enter Password");
		lblPW.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblPW.setBounds(53, 218, 96, 14);
		getContentPane().add(lblPW);
		
		JLabel lblPW1 = new JLabel("Re-enter Password");
		lblPW1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblPW1.setBounds(53, 267, 126, 14);
		getContentPane().add(lblPW1);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		passwordField_1.setBounds(189, 262, 150, 26);
		getContentPane().add(passwordField_1);
		
		btnregister.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnregister.setBounds(189, 378, 89, 23);
		getContentPane().add(btnregister);
		btnregister.addActionListener(this);
		
		JLabel lblAdd = new JLabel("Address");
		lblAdd.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblAdd.setBounds(53, 319, 96, 17);
		getContentPane().add(lblAdd);
		
		textArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textArea.setBounds(189, 299, 150, 60);
		getContentPane().add(textArea);
		
		JLabel lblUN = new JLabel("User Name");
		lblUN.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblUN.setBounds(53, 124, 96, 17);
		getContentPane().add(lblUN);
		
		textFieldUN = new JTextField();
		textFieldUN.setBounds(189, 120, 150, 26);
		getContentPane().add(textFieldUN);
		textFieldUN.setColumns(10);
		
		JLabel lblMN = new JLabel("Mobile Number");
		lblMN.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblMN.setBounds(53, 168, 96, 26);
		getContentPane().add(lblMN);
		
		textFieldMN = new JTextField();
		textFieldMN.setBounds(189, 166, 150, 26);
		getContentPane().add(textFieldMN);
		textFieldMN.setColumns(10);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnregister) {
			String FirstName = textFN.getText();
			String LastName = textLN.getText();
			String Password = null;
			String UserName = textFieldUN.getText();
			int mobNo = Integer.parseInt(textFieldMN.getText());
			String Address = textArea.getText();
			
			if(passwordField.getText().equals(passwordField_1.getText())) {
				 Password = passwordField.getText();
			}
			else
				System.out.println("Password is incorrect");
			
			
			RegisterPageCode rp = new RegisterPageCode();
			rp.insertUserTable(FirstName,LastName,Password,Address,UserName,mobNo);
			JOptionPane.showMessageDialog(this, "Registered Successfully ");
		}
	}
}