import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Register } from '../register';
import { MustMatch } from './MustMatch.validator';
import { RegisterService } from './../register.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  statusCode: number;
  registerdetailsObj: Register;
  registration_status: any;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router, private registerservice : RegisterService) 
  {
    this.statusCode = 400;
    this.registration_status ="";
    this.registerForm = this.formBuilder.group({
      userName: ['', Validators.required],
      //lastName: ['', Validators.required],
      // validates date format yyyy-mm-dd
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:  ['', [Validators.required, Validators.minLength(6)]],
      acceptTerms: [false, Validators.requiredTrue],
      number:['',[Validators.required,Validators.pattern(/^\d{10}$/)]]
  });
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      userName: ['', Validators.required],
      //lastName: ['', Validators.required],
      // validates date format yyyy-mm-dd
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:  ['', [Validators.required, Validators.minLength(6)]],
      acceptTerms: [false, Validators.requiredTrue],
      number:['',[Validators.required,Validators.pattern(/^\d{10}$/)]]
  });
}
get f() { return this.registerForm.controls; }

onSubmit() {
  this.submitted = true;
// stop here if form is invalid
  //if (this.registerForm.invalid) {
    //  return;
  //}
  // display form values on success
  //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  //this.registerForm.get('userName').value;
  let data = { userName :this.registerForm.get('userName').value,
              email:this.registerForm.get('email').value,
              password : this.registerForm.get('password').value,
              mobilenumber: this.registerForm.get('number').value};
  console.log(data);
  
  /*let data = { userName :"a",
  email:"a@g.com",
  password : "1234567891",
  mobilenumber:"1234567894"};*/
  const registerdetails = this.registerservice.postregister(data);
  
  registerdetails.subscribe((data : Register) =>{
    this.registerdetailsObj = data;
    console.log(data);
    this.registration_status=data.status;
  }); 
}
onReset() {
  this.submitted = false;
  this.registerForm.reset();
}
}
