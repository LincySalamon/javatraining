import { Injectable } from '@angular/core';
//import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/internal/Observable';
import { Signin } from './signin';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SigninService {
  loginUrl = 'http://localhost:3000/login';
  options: { };
  static logindetails: void;
      //create constructor to get an http instance
  constructor(private http: HttpClient) {}
  getLogin(logindetails): Observable<object>{
    debugger;
    this.options= {
      observe : 'body',
      responseType : 'json'
    };
    return this.http.get(this.loginUrl+"?username="+logindetails.username+"&password="+logindetails.password, this.options);
  }
}
