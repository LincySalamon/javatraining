import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Signin } from '../signin';
import { SigninService } from '../signin.service';

//import { User } from '../user';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  title = 'LoginForm';
  logindetailsObj: Signin;
  loginForm : FormGroup;
  submitted = false;
  signin_status : any;

  constructor(private formBuilder : FormBuilder ,private http : HttpClient,private router : Router, private signinService : SigninService) {
    this.signin_status = '';
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit(){
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {
    console.log("insumbit");
    this.submitted = true;
    /*if(this.loginForm.invalid){
      return;
    }*/
    debugger;
    let data = {username :this.loginForm.get('username').value,
                password: this.loginForm.get('password').value};
    
                //alert(JSON.stringify(this.loginForm, null,4));
    console.log(data);
  
    const logindetails =this.signinService.getLogin(data);
    
    logindetails.subscribe((data : Signin)=>{
        this.logindetailsObj = data;
        console.log(data);
        this.signin_status = data.status;
        if(this.logindetailsObj.status != "invaliduser"){
          this.router.navigate(["/bussearch"]);
        }
      });
  }
}