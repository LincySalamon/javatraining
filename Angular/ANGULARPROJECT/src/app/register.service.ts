import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  registerUrl = 'http://localhost:3000/register';
  options = {};
  static registerdetails:void;
  constructor(private http: HttpClient) { }

  postregister(registerformdata):Observable<object>{
    this.options={
      observe : 'body',
      responseType : 'Json'
    };
    //debugger;
    
    return this.http.post<any>(this.registerUrl, {userdetails:registerformdata});
    
  }
  
}

