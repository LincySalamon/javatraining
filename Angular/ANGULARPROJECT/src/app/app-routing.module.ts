import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { RegistrationComponent } from './registration/registration.component';
import { BussearchComponent } from './bussearch/bussearch.component';

const routes: Routes = [{path:'',component:SigninComponent},
  {path:'registration',component:RegistrationComponent}, {path:'bussearch',component:BussearchComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
