import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit,OnDestroy,OnChanges{

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
   console.log("changes occur");
  }
  ngOnDestroy(): void {
    console.log("onDEstroy Event");
  }

  ngOnInit(): void {
    console.log("On initializw event");
  }
  onclick(mail:string):void{
    alert('Your mail is'+mail+'we will contact you...!');
  }
}
