import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'DemoTemplateForm';
  Employee : Employee;
  ngOnInit(): void {
    this.Employee =
    {
      firstname:'Firstname',
      lastname: 'Lastname',
      email:'test@xyz.com'
    };
  }
 
  onSubmit(contactForm): void{
    console.log(contactForm.value);
    console.log(contactForm.mail);
  }
  onReset(contactForm):void{
    contactForm.resetForm();
  }
}
export class Employee{
  firstname: string;
  lastname: string;
  email: string;
}