import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'formExample';
 listOfCountry:country[] = [
    new country('1' ,'India'),
    new country('2' ,'Canada'),
    new country('3' ,'USA'),
    new country('4' ,'Australia'),
    new country('5' ,'England')
  ];
model:any={};

  onSubmit():void{
    alert('SUCCESS!!\n\n '+ JSON.stringify(this.model,null,4));
  }
}
export class country {
  id : string;
  name : string;

    constructor(id:string, name:string){
      this.id = id;
      this.name = name;
    }
}
