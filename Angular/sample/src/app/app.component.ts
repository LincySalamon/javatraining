import { Component } from '@angular/core';

@Component({
  selector: 'app-root1',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Project';
  ans = true;
  no = 5;
  num = 3;
  months =['January','February','March','April','May','June','July','August','September','October','November','December'];
  name = [{fname:'Lincy',lname:'Salamon'},
          {fname:'Latha',lname:'Arun'},
          {fname:'Suganya',lname:'Venkatesh'}];
  
  details =[{name:'Jadzia',rollno:'001',percentage:'90.5'},
            {name:'Tony',rollno:'002',percentage:'92.43'},
            {name:'Deona',rollno:'003',percentage:'90.10'},
            {name:'Jasper',rollno:'004',percentage:'89.75'},
            {name:'Jesrin',rollno:'005',percentage:'92.19'},
            {name:'Rijo',rollno:'006',percentage:'91.32'}];

  onchange(e: any):void{
    alert('changed'+e.target.value);
  }
}
