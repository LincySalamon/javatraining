package com.arrayListExercise;

import java.util.ArrayList;

public class StringColors {
	public static void main(String[] args) {
		ArrayList<String> al = new ArrayList<>();
		al.add("Red");	al.add("Blue");	al.add("Green");
		al.add("Purble");	al.add("Yellow");	al.add("Pink");
		System.out.println(al);
		//3.Insert an Element
		al.set(0, "Black");
		System.out.println(al);
		//4.Retrieve an Element
		String ele = al.get(2);
		System.out.println(ele);
		ele = al.get(4);
		System.out.println(ele);
		//5.Update the element
		al.set(0, "White");
		System.out.println(al);
		//6.Remove 3rd element
		al.remove(3);
		System.out.println(al);
		//7.Search an element in array
		if(al.contains("Purble"))
			System.out.println("It contains Blue");
		else
			System.out.println("It doesn't contains Blue");
	}
}
