package com.arrayListExercise;

import java.util.ArrayList;
import java.util.Iterator;

public class IterateArrayEle {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);	list.add(50); list.add(60);
		list.add(20);	list.add(30);
		System.out.println(list);
		Iterator i = list.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
	}
}
