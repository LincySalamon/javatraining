package InheritTask;

public class LocalTaxableWorker extends StateTaxableWorker  {
	public LocalTaxableWorker(String name, float payrate, float taxrate, float hours, String stateName,
			float stateTaxrate, String cityName, float cityTaxrate) {
		super(name, payrate, taxrate, hours, stateName, stateTaxrate);
		this.cityName = cityName;
		this.cityTaxrate = cityTaxrate;
	}
	String cityName;
	float cityTaxrate;
	public String workInfo() {
		return toString();
	}
	@Override
	public String toString() {
		return "LocalTaxableWorker [cityName=" + cityName + ", cityTaxrate=" + cityTaxrate + ", stateName=" + stateName
				+ ", stateTaxrate=" + stateTaxrate + ", name=" + name + ", payrate=" + payrate + ", taxrate=" + taxrate
				+ ", hours=" + hours + "]";
	}
	public double taxWithheld() {
		return (cityTaxrate * grossPay());
	}
}
