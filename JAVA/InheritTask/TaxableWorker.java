package InheritTask;

public class TaxableWorker {
	String name;
	float payrate;
	float taxrate;
	float hours;
	public TaxableWorker(String name, float payrate, float taxrate, float hours) {
		super();
		this.name = name;
		this.payrate = payrate;
		this.taxrate = taxrate;
		this.hours = hours;
	}
	public String workInfo() {
		return toString();
	}
	@Override
	public String toString() {
		return "TaxableWorker [name=" + name + ", payrate=" + payrate + ", taxrate=" + taxrate + "]";
	}
	public double grossPay() {
		return (payrate * hours);
	}
	public double taxWithheld() {
		return (float)(taxrate * grossPay());
	}
}
