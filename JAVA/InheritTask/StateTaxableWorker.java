package InheritTask;

public class StateTaxableWorker extends TaxableWorker {
	String stateName;
	float stateTaxrate;
	public StateTaxableWorker(String name, float payrate, float taxrate, float hours, String stateName,
			float stateTaxrate) {
		super(name, payrate, taxrate, hours);
		this.stateName = stateName;
		this.stateTaxrate = stateTaxrate;
	}
	@Override
	public String workInfo() {
		return toString();
	}
	@Override
	public String toString() {
		return "StateTaxableWorker [stateName=" + stateName + ", stateTaxrate=" + stateTaxrate + ", name=" + name
				+ ", payrate=" + payrate + ", taxrate=" + taxrate + ", hours=" + hours + "]";
	}
	@Override
	public double taxWithheld() {
		return (stateTaxrate*grossPay());
	}
}
