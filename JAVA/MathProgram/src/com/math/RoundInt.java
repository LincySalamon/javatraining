package com.math;

public class RoundInt {
	int m1,m2,m3,m4,m5;
	public RoundInt(int m1, int m2, int m3, int m4, int m5) {
		super();
		this.m1 = m1;
		this.m2 = m2;
		this.m3 = m3;
		this.m4 = m4;
		this.m5 = m5;
	}
	public static void main(String[] args) {
		RoundInt r = new RoundInt(88, 78, 60, 90, 92);
		System.out.println("Total marks " + r.m1 +" "+r.m2 + " " +r.m3+ " " + r.m4+ " " +r.m5);
		System.out.println("Percentage " + ((r.m1+r.m2+r.m3+r.m4+r.m5)*100)/500);
	}
}
