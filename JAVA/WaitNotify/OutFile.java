package WaitNotify;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OutFile {
	public static void main(String[] args) {
		
		try {
			FileOutputStream out = new FileOutputStream("D:\\test1.txt");
			out.write(65);
			out.write(66);
			out.write(67);
			String str ="This is the content I write in text";
			byte[] arr = str.getBytes();
			out.write(arr);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			e1.printStackTrace();
		}
	}
}