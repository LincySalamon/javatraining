package WaitNotify;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileLineRead {
	public static void main(String[] args) {
		int noWords = 0;
		int noLines = 0;
	try {
		File f = new File("D:\\test.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String s;
		while((s=br.readLine())!=null)
			{
				System.out.println(s);
				String words[] = s.split(" ");
					noWords += words.length;
				String[] lines = s.split("\n");
					noLines += lines.length;
			}
		System.out.println("Numer of Words in a file is "+noWords);
		System.out.println("Numer of Lines in a file is "+noLines);
		br.close();
		}
	catch(FileNotFoundException e)
	{
		e.printStackTrace();
	}
	catch(IOException e1)
	{
		e1.printStackTrace();
	}
	}
}
