package WaitNotify;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class IOfileEx {
	public static void main(String[] args) {
		int digitcount=0,nocount=0;
		try {
		FileInputStream in = new FileInputStream("D:\\test.txt");
		int i = 0;
		while((i = in.read())!=-1)
			{
				if(Character.isDigit((char)i))
					digitcount++;
				if(Character.isAlphabetic((char)i))
					nocount++;
			}
		System.out.println("Number of Digits in file is "+digitcount);
		System.out.println("Number of Alphabets in file is "+nocount);
		in.close();
		}catch(FileNotFoundException e)
		{
			System.out.println("File does not exist");
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			System.out.println("File cannot be read");
			e1.printStackTrace();
		}
	}
}
