package com.shapes;

public class Test {
	public static void main(String[] args) {
		Circle c = new Circle("Circle", 7);
		System.out.println(c);
		System.out.println(c.area());
		System.out.println(c.peri());
		
		Rectangle r = new Rectangle("RectAngle", 7,10);
		System.out.println(r);
		System.out.println(r.area());
		System.out.println(r.peri());
	}
}
