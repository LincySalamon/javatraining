package com.shapes;

public class Rectangle extends Shapes {
	int l,b;
	public Rectangle(String name, int l, int b) {
		super(name);
		this.l = l;
		this.b = b;
	}
	@Override
	public float area() {
		return (float) (l*b);
	}
	@Override
	public float peri() {
		return (float) (2*(l+b));
	}
	@Override
	public String toString() {
		return "Rectangle [l=" + l + ", b=" + b + ", name=" + name + "]";
	}
	
}
