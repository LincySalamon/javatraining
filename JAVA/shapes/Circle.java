package com.shapes;

public class Circle extends Shapes{
	int rad;

	public Circle(String name, int rad) {
		super(name);
		this.rad = rad;
	}
	@Override
	public float area() {
		return (float) (3.143*rad*rad);
	}
	@Override
	public float peri() {
		return (float) (2*3.143*rad);
	}
	@Override
	public String toString() {
		return "Circle [rad=" + rad + ", name=" + name + "]";
	}
}
