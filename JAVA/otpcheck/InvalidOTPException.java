package com.otpcheck;

public class InvalidOTPException extends Exception {

	public InvalidOTPException(String message) {
		super(message);
		
	}
	
}
