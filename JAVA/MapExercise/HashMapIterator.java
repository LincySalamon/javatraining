package MapExercise;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class HashMapIterator {
	public static void main(String[] args) {
		HashMap<Integer,String> map = new HashMap<>();
		map.put(1, "Jad");
		map.put(2, "Tony");
		map.put(3, "Jas");
		map.put(4, "Deo");
		System.out.println(map);
		for(Entry<Integer, String> e : map.entrySet())
		{
			System.out.println("Key "+e.getKey()+" Value "+e.getValue());
		}
		for(Integer i : map.keySet())
		{
			System.out.println("key "+i);
		}
		for(String s : map.values())
		{
			System.out.println("Value "+s);
		}
		Iterator<Map.Entry<Integer,String>> i = map.entrySet().iterator();
		while(i.hasNext())
		{
			Entry<Integer, String> itr = i.next();
			System.out.println("Key "+itr.getKey()+"Value "+itr.getValue());
		}
	}
}
