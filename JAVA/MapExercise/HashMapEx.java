package MapExercise;

import java.util.HashMap;

public class HashMapEx {
	public static void main(String[] args) {
		HashMap<Integer,String> map = new HashMap<>();
		map.put(1, "Jad");
		map.put(2, "Tony");
		map.put(3, "Jas");
		map.put(4, "Deo");
		System.out.println(map);
		System.out.println(map.size());
		System.out.println(map.entrySet());
		System.out.println(map.keySet());
		System.out.println(map.values());
		System.out.println(map.get(3));
		System.out.println(map.remove(4));
		System.out.println(map.containsKey(5));
		System.out.println(map.containsValue("Jad"));
		map.clear();
		System.out.println(map);
	}
}
