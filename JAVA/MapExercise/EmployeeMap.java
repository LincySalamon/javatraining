package MapExercise;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

import com.collectionEx.EmployeeHashTree;
public class EmployeeMap {
	public static void main(String[] args) {
		HashMap<Integer, EmployeeHashTree> map = new HashMap<>();
		EmployeeHashTree e1 = new EmployeeHashTree(101, "Lincy", 50000);
		EmployeeHashTree e2 = new EmployeeHashTree(102, "Arun", 100000);
		EmployeeHashTree e3 = new EmployeeHashTree(103, "Latha", 50000);
		EmployeeHashTree e4 = new EmployeeHashTree(104, "Salamon", 1050000);
		map.put(1, e1);	map.put(2, e2);	map.put(3, e3);	map.put(4, e4);
		map.put(5, new EmployeeHashTree(105, "Tom", 60000) );
		for(Entry<Integer, EmployeeHashTree> m : map.entrySet())
		{
			System.out.println(m);
		}
		Scanner s = new Scanner(System.in);
		System.out.println("ENter the name");
		String str = s.next();
		
		if(map.containsValue(str))
		{
			System.out.println("Name has already Exist");
		}
		else
		{
			map.put(6, new EmployeeHashTree(106, str,10000));
			System.out.println("Add to map");
		}
		System.out.println(map);
	}
}
