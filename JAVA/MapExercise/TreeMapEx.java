package MapExercise;

import java.util.Collections;
import java.util.TreeMap;

public class TreeMapEx {
	public static void main(String[] args) {
		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(10, "dddd");
		map.put(20, "gggg");
		map.put(50, "aaaa");
		map.put(5, "eeee");
		map.put(1, "bbbb");
		System.out.println(map);
		
		TreeMap<String, String> map1 = new TreeMap<>(Collections.reverseOrder());
		map1.put("pqr", "dddd");
		map1.put("lmn", "gggg");
		map1.put("abc", "aaaa");
		map1.put("zzz", "eeee");
		map1.put("tes", "bbbb");
		System.out.println(map1);
	}
}
