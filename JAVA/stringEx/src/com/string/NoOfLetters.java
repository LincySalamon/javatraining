package com.string;

import java.util.Scanner;

public class NoOfLetters {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String s1 = s.nextLine();
		s.close();
		int count=0;
		System.out.println(s1);
		char str[] = s1.toCharArray();
		System.out.println(str);
		for(int i=0; i<str.length; i++)
		{
			if(str[i]>=65 && str[i]<=90 && str[i]<=122 && str[i]>=97)
			{
				count++;
			}
		}
		System.out.println("No.of letters in the string is "+count);
}
}