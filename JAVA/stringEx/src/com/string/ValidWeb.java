package com.string;

import java.util.Scanner;

public class ValidWeb {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the website link ");
		String str = s.next();
		s.close();
		
		String regEx = "((https|http)://)(www.)?[a-zA-Z0-9]{2,256}\\.[a-z]{2,3}";
		System.out.println(str.matches(regEx));
	}
}
