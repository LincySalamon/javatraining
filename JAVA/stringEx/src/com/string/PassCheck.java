package com.string;
import java.util.Scanner;

public class PassCheck {
	   public static void main(String args[]) {
	  Scanner s = new Scanner(System.in);
	  String pass ;
	  System.out.println("Enter your password");
	  pass = s.next();
	  s.close();
	  PassCheck m = new PassCheck();
	  if(m.isValid(pass))
		  System.out.println("correct password");
	  else {
		  System.out.println("Incorrect password");
	      System.out.println("Password must have atleast one digit, one uppercase "
		 		+ "and one special character");
	  }
	   }
public boolean isValid(String pass)
	   {
		 boolean isUp = false,isDi = false,isSpl=false; 
	   
		   if(pass.length()<8)
		   {
			   System.out.println("Invalid length");
			   return false;
		   }
		 	  char arr[] = pass.toCharArray();
		 	  for(int i = 0;i<arr.length;i++)
		 	  {
		        if(Character.isDigit(arr[i])== true) {
		        	isDi = true;
		        }
		        if(Character.isUpperCase(arr[i]))
		        	isUp = true;
		        if(arr[i]=='!' ||arr[i]=='@' ||arr[i]=='#'||arr[i]=='*'  )
		        	isSpl = true;
		 	  }
		 	  if(isDi==true && isSpl == true && isUp == true)
		 		  return true;
		 	 else 
		 		   return false;
		 	  
		 	  }
	}