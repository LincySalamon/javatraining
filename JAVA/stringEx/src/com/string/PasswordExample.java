package com.string;
import java.util.Scanner;
	public class PasswordExample {  
	    public static boolean isValid(String password) 
	    { 
	        if (!((password.length() >= 8) 
	              && (password.length() <= 15))) { 
	            return false; 
	        } 
	   
	        if (password.contains(" ")) { 
	            return false; 
	        } 
	        if (true) { 
	            int count = 0; 
	            for (int i = 0; i <= 9; i++) { 
	            	String str1 = Integer.toString(i); 
	            	if (password.contains(str1)) {
	                	count = 1; 
	                } 
	            } 
	            if (count == 0) { 
	                return false; 
	            } 
	        } 
	        // for special characters 
	        if (!(password.contains("@") || password.contains("#") 
	              || password.contains("~") || password.contains("$")  
	              || password.contains("&") || password.contains("*")  
	              || password.contains(":") || password.contains(".") 
	              || password.contains(", ")|| password.contains("?") )) { 
	            return false; 
	        } 
	        if (true) { // checking capital letters
	            int count = 0; 
	            for (int i = 65; i <= 90; i++) { 
	            	  // type casting
	            	char c = (char)i; 
	            	String str1 = Character.toString(c); 
	                if (password.contains(str1)) { 
	                    count = 1; 
	                } 
	            } 
	            if (count == 0) { 
	                return false; 
	            } 
	        } 
	        if (true) {  // checking small letters 
	            int count = 0; 
	            for (int i = 90; i <= 122; i++) { 
	            	// type casting 
	                char c = (char)i; 
	                String str1 = Character.toString(c); 
	                if (password.contains(str1)) { 
	                    count = 1; 
	                } 
	            } 
	            if (count == 0) { 
	                return false; 
	            } 
	        } 
	        // if all conditions fails 
	        return true; 
	    }
	    public static void main(String[] args) 
	    { 
	    	String password1 = "Allisgood"; 
	    	if (isValid(password1)) { 
	            System.out.println("Valid Password"); 
	        } 
	        else { 
	            System.out.println("Invalid Password!!!"); 
	        } 
	    	String password2 = "AllisGood@7"; 
	        if (isValid(password2)) { 
	            System.out.println("Valid Password"); 
	                }
	        else { 
	            System.out.println("Invalid Password!!!"); 
	        } 
	    } 
	} 
