package com.string;

import java.util.Scanner;

public class ReplaceWord {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the string");
		String str = s.nextLine();
		s.close();
		str = str.replaceAll("java", "j2ee");
		System.out.println("After replacing: "+str);
		
	}
}
