package com.string;

import java.util.Scanner;

public class NoOfChar {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String str = s.nextLine();
		s.close();
		//NoOfChar obj = new NoOfChar();
		
		int up=0, low=0, spl=0;
		char arr[] = str.toCharArray();
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i]>=65 && arr[i]<=90)
				up++;
			else if(arr[i]>=97 && arr[i]<=122)
				low++;
			else if(arr[i]=='@' || arr[i]=='#' || arr[i]=='$' || arr[i]=='&' || arr[i]=='*')
				spl++;
		} 
		if(up > 0 || low>0 || spl>0)
			System.out.println("No.of Upper case is "+up+"\nNo.of Lower case is "+low+"\nNo.of Special Char is "+spl);
}}

