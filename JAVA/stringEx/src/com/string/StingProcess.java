package com.string;

import java.util.StringTokenizer;

public class StingProcess {
	public static void main(String[] args) {
		String s1 = "Chennai";
		
		StringBuffer sb = new StringBuffer("Chennai");
		StringBuilder s = new StringBuilder("India");
		StringTokenizer st = new StringTokenizer("new string");
		
		s1 = s1.concat("India");
		System.out.println(s1);
		
		System.out.println(sb);
		sb.append("India");
		System.out.println(sb);
		System.out.println(s);
		s.append("Asia");
		System.out.println(s);
	}
}
