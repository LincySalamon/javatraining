package com.string;

public class RegisterExample {
	public static void main(String[] args) {
		String ch ="A1";
		String regEx = "[A-Z][0-9]";
		System.out.println(ch.matches(regEx));
		
		String ch1="123456";
		String regex ="[0-9]*";//it check only number
		System.out.println(ch1.matches(regex));
		
		String ch2="abcAbc";
		String regex1 ="[A-Za-z]*";//it check only letter 
		System.out.println(ch2.matches(regex1));
		
		String ch3="AbC1";
		String regex2 ="[A-Za-z0-9]*";//it check only all case letter and number
		System.out.println(ch3.matches(regex2));
	}
}
