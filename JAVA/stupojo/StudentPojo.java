package com.stupojo;

public class StudentPojo {
	private int rollno;
	private String stuname;
	private float stuPerc;
	@Override
	public String toString() {
		return "StudentPojo [rollno=" + rollno + ", stuname=" + stuname + ", stuPerc=" + stuPerc + "]";
	}
	public StudentPojo() {
		super();
	}
	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

	public String getStuname() {
		return stuname;
	}

	public void setStuname(String stuname) {
		this.stuname = stuname;
	}

	public float getStuPerc() {
		return stuPerc;
	}

	public void setStuPerc(float stuPerc) {
		this.stuPerc = stuPerc;
	}
	
}
