package com.stupojo;
import java.util.ArrayList;

public class StudentPojoTest {
	public static void main(String args[]) {
		
		StudentPojo s = new StudentPojo();
		s.setRollno(100);
		s.setStuname("Anu");
		s.setStuPerc(98.4f);
		StudentPojo s1 = new StudentPojo();
		s1.setRollno(101);
		s1.setStuname("Anish");
		s1.setStuPerc(78.76f);
		StudentPojo s2 = new StudentPojo();
		s2.setRollno(102);
		s2.setStuname("Augusty");
		s2.setStuPerc(95.76f);
		StudentPojo s3 = new StudentPojo();
		s3.setRollno(103);
		s3.setStuname("Balu");
		s3.setStuPerc(65.77f);
		StudentPojo s4 = new StudentPojo();
		s4.setRollno(104);
		s4.setStuname("Guru");
		s4.setStuPerc(88.76f);
		ArrayList<StudentPojo> list = new ArrayList<>();
		list.add(s); list.add(s1);	list.add(s2);
		list.add(s3);	 list.add(s4);
		
		for(int i=0; i<list.size();i++)
			System.out.println(list.get(i));
	}
}
