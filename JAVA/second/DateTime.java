package com.second;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

public class DateTime {  
		  public static void main(String[] args) {  
		    
			LocalDate date = LocalDate.now();  
		    LocalDate yesterday = date.minusDays(1);  
		    LocalDate tomorrow = yesterday.plusDays(2);  
		    
		    System.out.println("Today date: "+date);  
		    System.out.println("Yesterday date: "+yesterday);  
		    System.out.println("Tommorow date: "+tomorrow);  
		    
		    LocalDate date1 = LocalDate.of(2003,5,13);
		    System.out.println(date1.isLeapYear());
		    LocalDate date2 = LocalDate.of(2000, 6, 20);
		    System.out.println(date2.isLeapYear());
		    
		    LocalTime time = LocalTime.now();
		    	System.out.println("Local TIme is " + time);
		    LocalTime time1 = LocalTime.of(10,43,12);  
		    	System.out.println(time1);  
		    LocalTime time2=time1.minusHours(2);  //Like that plusMinutes and plusHours
		    LocalTime time3=time2.minusMinutes(34);  
		    	System.out.println(time3);
		   
		    LocalDateTime now = LocalDateTime.now();  
		        System.out.println("Before Formatting: " + now);  
		    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
		        String formatDateTime = now.format(format);  
		        System.out.println("After Formatting: " + formatDateTime);
		    	
		    ZoneId zone1 = ZoneId.of("Asia/Kolkata");  
		    ZoneId zone2 = ZoneId.of("Asia/Tokyo");  
		    LocalTime time4 = LocalTime.now(zone1);  
		        System.out.println("India Time Zone: "+time4);  
		    LocalTime time5 = LocalTime.now(zone2);  
		        System.out.println("Japan Time Zone: "+time5);
		    //it is a set of array that contains many like miles hours, min,day,week etc
		    long hours = ChronoUnit.HOURS.between(time4, time5); 
		        System.out.println("Hours between two Time Zone: "+hours);  
		    long minutes = ChronoUnit.MINUTES.between(time4, time5);  
		        System.out.println("Minutes between two time zone: "+minutes); 
		        
		    LocalDateTime a = LocalDateTime.of(2017, 2, 13, 15, 56);    
		        System.out.println(a.get(ChronoField.DAY_OF_WEEK));  
		        System.out.println(a.get(ChronoField.DAY_OF_YEAR));  
		        System.out.println(a.get(ChronoField.DAY_OF_MONTH));  
		        System.out.println(a.get(ChronoField.HOUR_OF_DAY));  
		        System.out.println(a.get(ChronoField.MINUTE_OF_DAY));  
		        
		     
		      } 
		} 