package com.second;

import java.util.Scanner;

public class ThrowEx1 {
	public static void main(String[] args) {
		String s = null;
		try {
			if(s==null) 
				throw new NullPointerException("String is null");
			else
				System.out.println("Good to go");
		}
			catch(NullPointerException e)
			{
				System.err.println("Age should be positive");
			}
		}
	}

