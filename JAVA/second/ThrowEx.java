package com.second;

import java.util.Scanner;

public class ThrowEx {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter your age");
		int age = s.nextInt();
		s.close();
		try {
			if(age < 0) 
				throw new ArithmeticException();
			else
				System.out.println("Age is valid");
		}
			catch(ArithmeticException e)
			{
				System.err.println("Age should be positive");
			}
		}
	}
