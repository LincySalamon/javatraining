package InterEx;

public class SUV implements vehicle{
	public SUV(String carname, int price, int year) {
		super();
		this.carname = carname;
		this.price = price;
		this.year = year;
	}
	String carname;
	int price;
	int year;
	
	@Override
	public void printData() {
		System.out.println(toString());
		
	}
	@Override
	public String toString() {
		return "SUV [carname=" + carname + ", price=" + price + ", year=" + year + "]";
	}

}
