package InterEx;

public class Car implements vehicle {
	String carname;
	int price;
	int year;
	
	public Car(String carname, int price, int year) {
		super();
		this.carname = carname;
		this.price = price;
		this.year = year;
	}
	@Override
	public void printData() {
		System.out.println(toString());
	}
	@Override
	public String toString() {
		return "Car [carname=" + carname + ", price=" + price + ", year=" + year + "]";
	}
}
