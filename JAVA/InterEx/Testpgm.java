package InterEx;

public class Testpgm {
	public static void main(String[] args) {
		Car c = new Car("Hyundai", 700000, 2017);
		SUV s = new SUV("Innova", 1500000, 2015);
		Minivan m = new Minivan("Enjoy", 2500000, 2018);
		c.printData();
		s.printData();
		m.printData();
	}
}
