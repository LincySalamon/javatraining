package InterEx;

public class Minivan implements vehicle{
	public Minivan(String carname, int price, int year) {
		super();
		this.carname = carname;
		this.price = price;
		this.year = year;
	}
	String carname;
	int price;
	int year;
	@Override
	public void printData() {
		System.out.println(toString());
	}
	@Override
	public String toString() {
		return "Minivan [carname=" + carname + ", price=" + price + ", year=" + year + "]";
	}

}
