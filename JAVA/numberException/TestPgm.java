package com.numberException;

import java.util.Scanner;

public class TestPgm {
	//double f;
	public double toConvert(float cel) throws NumberConversionException{
		double f = (cel*1.8) + 32;
		if(f==0 || f<0) {
			throw new NumberConversionException("No negative temperature in our area");
		}
		return f;
	}
	public static void main(String[] args) {
		TestPgm t = new TestPgm();
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Celcius value");
		float cel = s.nextFloat();
		s.close();
		try {
				System.out.println("Cel to Farenheit is "+t.toConvert(cel));
		}
		catch(NumberConversionException e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
