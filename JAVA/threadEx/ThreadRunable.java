package com.threadEx;

public class ThreadRunable implements Runnable {
	public static void main(String[] args) {
		ThreadRunable obj = new ThreadRunable();
		Thread t = new Thread(obj);
		t.start();
		Thread t1 = new Thread(obj);
		t1.start();
	}

	@Override
	public void run() {
		try
		{
			for(int i=0;i<=10;i++)
			{
				System.out.println(i);
				Thread.sleep(1000);
			}
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
	}
}
