package com.UserDefinedSort;

import java.util.ArrayList;
import java.util.Collections;

public class Main1 {
	public static void main(String[] args) {
		ArrayList<Student1> list = new ArrayList<>();
		list.add(new Student1(20, "lmn", 56.32f));
		list.add(new Student1(10, "pqr", 45.87f));
		list.add(new Student1(5, "abc", 67.54f));
		list.add(new Student1(25, "fgh", 98.2f));
		Collections.sort(list,new NameComparator());
		System.out.println("After name sorting..");
		for(Student1 s : list)
		{
			System.out.println(s);
		}
		Collections.sort(list,new rollNoComparator());
		System.out.println("After rollNo sorting..");
		for(Student1 s : list)
		{
			System.out.println(s);
		}
		Collections.sort(list,new PercComparator());
		System.out.println("After percentage sorting..");
		for(Student1 s : list)
		{
			System.out.println(s);
		}
	}
}
