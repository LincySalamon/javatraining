package com.UserDefinedSort;

import java.util.Comparator;

public class rollNoComparator implements Comparator<Student1> {

	@Override
	public int compare(Student1 o1, Student1 o2) {
		if(o1.rollNo == o2.rollNo)
			return 0;
		else if(o1.rollNo > o2.rollNo)
			return 1;
		else
			return -1;
	}

}
