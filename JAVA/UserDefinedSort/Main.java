package com.UserDefinedSort;

import java.util.ArrayList;

import java.util.Collections;

public class Main {
	public static void main(String[] args) {
		ArrayList<Student> list = new ArrayList<>();
		list.add(new Student(20, "lmn", 56.32f));
		list.add(new Student(10, "pqr", 45.87f));
		list.add(new Student(5, "abc", 67.54f));
		list.add(new Student(25, "fgh", 98.2f));
		for(Student s : list)
		{
			System.out.println(s);
		}
		Collections.sort(list);
		System.out.println("After sorting....");
		int rank=1;
		for(Student s : list)
		{
			System.out.println(s.studName + " got "+(rank++)+" rank with "+s.studPerc);
		}
		/*Collections.reverse(list);
		System.out.println("Reverse sorting....");
		for(Student s : list)
		{
			System.out.println(s);
		}*/
	}
}
