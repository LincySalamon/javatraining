package com.UserDefinedSort;

import java.util.Comparator;

public class PercComparator implements Comparator<Student1> {

	@Override
	public int compare(Student1 o1, Student1 o2) {
		if(o1.studPerc == o2.studPerc)
			return 0;
		else if(o1.studPerc > o2.studPerc)
			return -1;
		else
			return 1;
	}

}
