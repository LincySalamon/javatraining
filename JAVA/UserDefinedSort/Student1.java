package com.UserDefinedSort;

public class Student1 {
	public Student1(int rollNo, String studName, float studPerc) {
		super();
		this.rollNo = rollNo;
		this.studName = studName;
		this.studPerc = studPerc;
	}
	public int rollNo;
	public String studName;
	public float studPerc;
	
	@Override
	public String toString() {
		return "Student1 [rollNo=" + rollNo + ", studName=" + studName + ", studPerc=" + studPerc + "]";
	}
}
