package com.UserDefinedSort;

public class Student implements Comparable<Student> {
	public int rollNo;
	public String studName;
	public float studPerc;
	public Student(int rollNo, String studName, float studPerc) {
		super();
		this.rollNo = rollNo;
		this.studName = studName;
		this.studPerc = studPerc;
	}
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", studName=" + studName + ", studPerc=" + studPerc + "]";
	}
	/*@Override
	public int compareTo(Student o) {
		return studName.compareTo(o.studName);
	}*/
	@Override
	public int compareTo(Student o) {
		if(studPerc == o.studPerc)
			return 0;
		else if(studPerc > o.studPerc)
			return -1;
		else
			return 1;
	}
	
}
