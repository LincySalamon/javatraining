package MultiThreadEx;

public class ThreadA extends Thread {
	public void run()
	{
		try {
			String name = currentThread().getName();
			System.out.println("Hello from "+name);
			sleep(1000);
			}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
