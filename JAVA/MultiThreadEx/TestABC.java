package MultiThreadEx;

public class TestABC {
	public static void main(String[] args) {
		ThreadA a = new ThreadA();
		a.setName("Thread A");
		a.start();
		ThreadB b = new ThreadB();
		b.setName("Thread B");
		b.start();
		ThreadC c = new ThreadC();
		c.setName("Thread c");
		c.start();
	}
}
