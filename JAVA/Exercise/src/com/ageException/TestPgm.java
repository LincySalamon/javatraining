package com.ageException;

public class TestPgm {
	public static void main(String[] args) {
		int age = -15;
		try {
			if(age<0)
				throw new AgeException("Age cannot be -ve");
			else
				System.out.println("Correct Age..");
		}
		catch(AgeException e)
		{
			System.out.println(e);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
