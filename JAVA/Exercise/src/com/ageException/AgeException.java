package com.ageException;

public class AgeException extends Exception{
	public AgeException(String s) {
		super(s);
	}

	@Override
	public String toString() {
		return "AgeException [toString()="+super.toString()+"]";
	}
	
}
