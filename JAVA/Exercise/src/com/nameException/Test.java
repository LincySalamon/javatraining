package com.nameException;

import java.util.Scanner;

public class Test {
	public boolean isValid(String name)throws InvalidNameException
	{
		for(int i =0; i<name.length(); i++)
		{
			char ch = name.charAt(i);
				if(!((ch>='a' && ch<='z')||(ch>='A' && ch<='Z')))
				{
					throw new InvalidNameException("Name have not numbers and special characters");
				}
		}
		return true;
	}
	public static void main(String[] args) {
		Test t = new Test();
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Your Name");
		String name = s.next();
		//System.out.println(name.length());
		s.close();
		try {
			if(t.isValid(name))
				System.out.println("Correct Name");
			}
		catch(InvalidNameException e)
		{
			System.out.println(e);
			//System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
