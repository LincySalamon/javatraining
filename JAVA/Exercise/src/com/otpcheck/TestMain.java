package com.otpcheck;

import java.util.Scanner;

public class TestMain {
	public boolean checkOTP(int num)throws InvalidOTPException
	{
		//int randomNo = 7896;
		int randomNo = (int) (Math.random() *(9999-1000)+1000) ;
		System.out.println("Generated OTP is "+randomNo);
		if(!(num == randomNo) ){
			throw new InvalidOTPException("OTP must have a 4 digit only or not matched with random numer");}
		
		return true;
	}
	public static void main(String[] args) {
		TestMain tm = new TestMain();
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the OTP");
		int num = s.nextInt();
		s.close();
		try {
			if(tm.checkOTP(num)) {
				System.out.println("Entered OTP is Correct");
		} }
		catch (InvalidOTPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
