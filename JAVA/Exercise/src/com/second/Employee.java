package com.second;

public class Employee {
	int empId;
	String name;
	float sal;
	float insen;
	int ded;
	
	public Employee() {
		super();
	}
	public Employee(int empId, String name, float sal, float insen, int ded) {
		super();
		this.empId = empId;
		this.name = name;
		this.sal = sal;
		this.insen = insen;
		this.ded = ded;
	}
	public Employee(int empId, String name, float sal, float insen) {
		super();
		this.empId = empId;
		this.name = name;
		this.sal = sal;
		this.insen = insen;
	}
	public Employee(int empId, String name, float sal, int ded) {
		super();
		this.empId = empId;
		this.name = name;
		this.sal = sal;
		this.ded = ded;
	}
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", name=" + name + ", sal=" + sal + ", insen=" + insen + ", ded=" + ded
				+ "]";
	}
	public static void main(String[] args) {
		Employee em = new Employee();
		Employee em1 = new Employee(001,"Tommy",15000.500f,10000.00f,2000);
		Employee em2 =  new Employee(002,"Hills",20000.00f,12000.500f);
		Employee em3 = new Employee(003,"Andrew",25000.0f,3000);
		System.out.println(em + "\n" + em1 + "\n" + em2 + "\n" + em3 + "\n");
		em.calTax(25000.0f,10000.500f,2000);
	}
	private void calTax(float sal,float insen,int ded) {
		double tax = (sal + insen - ded) * 0.1;
		System.out.println("Calculated Tax " + tax);
	}

}