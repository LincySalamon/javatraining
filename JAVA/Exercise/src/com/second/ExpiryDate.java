package com.second;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ExpiryDate {
	public static void main(String[] args) {
		DateTimeFormatter myFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		//LocalDate d = LocalDate.now();
		//System.out.println(d);
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Manufacturing date with format of (dd-mm-yyyy)");
		String mfg_date = s.next();
		
		LocalDate mfgDate = LocalDate.parse(mfg_date,myFormat); // convert mfg string to date
		
		LocalDate expDate = mfgDate.plusMonths(6);
		//LocalDate exp_date = LocalDate.parse(expDate, myFormat);
		 System.out.println("Expiry Date is " + expDate);
	}
}