package com.second;

public class trycatch {
	public static void main(String[] args) {
		int i =50;
		try
		{
			System.out.println("Before divison");
			float ans = i/0;
			System.out.println(ans);
			System.out.println("Good job....");
		}
		catch(ArithmeticException e)
		{
			System.out.println("We cannot divide by 0. Try again");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		System.out.println("After Divison");
	}
}