package com.second;

import java.io.IOException;

public class ThrowEx2 {
	public void doDivision() throws IOException
	{
		System.out.println("All good");
		throw new IOException();
	}
	public static void main(String[] args) {
		ThrowEx2 t = new ThrowEx2();
		try {
			t.doDivision();
		}
		catch(IOException e1)
		{
			e1.printStackTrace();
		}
	}
}
