package com.numberException;

public class NumberConversionException extends Exception {

	public NumberConversionException(String message) {
		super(message);
	}
	
}
