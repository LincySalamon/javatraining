package com.collectionEx;

import java.util.LinkedList;

public class LinkedListEx {
	public static void main(String[] args) {
		LinkedList<Integer> ll = new LinkedList<>();
		ll.add(100);	ll.add(500);	ll.add(200);
		ll.add(300);	ll.add(1000);
		System.out.println(ll);
		System.out.println(ll.get(3));
		ll.set(2, 700);
		System.out.println(ll);
		ll.remove(3);
		System.out.println(ll);
		System.out.println(ll.indexOf(1000));
		
		for(int i=0; i< ll.size(); i++)
			System.out.println(ll.get(i));
	}
}
