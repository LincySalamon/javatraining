package com.collectionEx;

import java.util.HashSet;
import java.util.Iterator;
public class HashSetOperations {
	public static void main(String[] args) {
		
	HashSet<Integer> set = new HashSet<>();
	set.add(Integer.valueOf(10));
	Integer i = new Integer(15);
	set.add(i);
	set.add(500);
	set.add(105);
	set.add(Integer.valueOf(25));
	for(Integer no:set)
		System.out.println(no);
	System.out.println("Print with help of Iterator");
	Iterator<Integer> x = set.iterator();
	while(x.hasNext())
	{
		System.out.println(x.next());
	}
	set.remove(i);
		System.out.println(set);
	System.out.println(set.size());
	if(set.contains(10))
		System.out.println("It contains");
	else
		System.out.println("It doesn't contains");
	if(set.isEmpty())
		System.out.println("Empty");
	else
		System.out.println("Not Empty");
	}
}
