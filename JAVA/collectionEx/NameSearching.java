package com.collectionEx;

import java.util.HashSet;

public class NameSearching {
	public static void main(String[] args) {
		HashSet<String> set = new HashSet<>();
		set.add("Bala");
		set.add(new String("Arun"));
		set.add("Aruna");
		set.add(new String("Suganya"));
		set.add("Venkat");
		System.out.println(set);
		for(String str:set)
		{
			if(str.equals("Arun"))
				System.out.println("It contains Arun");
			//System.out.println(str);
		}
		if(set.contains("Arun"))
			System.out.println("It contains Arun");
		else
			System.out.println("It doesn't contains");
	}
}
