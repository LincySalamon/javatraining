package com.collectionEx;

import java.util.HashSet;
import java.util.Iterator;

public class EmployeeTest {
	public static void main(String[] args) {
		EmployeeHashTree e1 = new EmployeeHashTree(10, "Arjun", 10000);
		EmployeeHashTree e2 = new EmployeeHashTree(20, "Anish", 30000);
		EmployeeHashTree e3 = new EmployeeHashTree(30, "Tommy", 20000);
		EmployeeHashTree e4 = new EmployeeHashTree(40, "John", 60000);
		EmployeeHashTree e5 = new EmployeeHashTree(50, "Joshua", 70000);
		
		HashSet<EmployeeHashTree> set = new HashSet<EmployeeHashTree>();
		set.add(e1);	set.add(e2);
		set.add(e3);	set.add(e4);	set.add(e5);
		
		for(EmployeeHashTree e: set)
			System.out.println(e);
		
		Iterator i = set.iterator();
		while(i.hasNext())
		{
			EmployeeHashTree e = (EmployeeHashTree) i.next();
			System.out.println(e.empId+" "+e.empName+" "+e.empSalary);
		}
	}
}
