package com.collectionEx;

import java.util.TreeSet;

public class TreeSetEx {
	public static void main(String[] args) {
		TreeSet<Integer> set = new TreeSet<>();
		set.add(Integer.valueOf(10));
		Integer i = new Integer(15);
		set.add(i);
		set.add(500);
		set.add(105);
		set.add(Integer.valueOf(25));
		System.out.println(set);
		set.add(1000);
		System.out.println(set.descendingSet());
		System.out.println(set.first());
		System.out.println(set.last());
	}
}
