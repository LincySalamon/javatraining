package com.collectionEx;

import java.util.ArrayList;
import java.util.Collections;

public class StrinArrayList {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("amutha");	list.add("aruna");	list.add("tamil");
		list.add("latha");	list.add("lincy");
		System.out.println(list);
		 
		for(int i = list.size()-1 ; i>=0; i--)
	    	 System.out.println(list.get(i));
		
		Collections.reverse(list);
			System.out.println(list);
		
		list.sort(null);
		System.out.println(list);
		
		Collections.sort(list,Collections.reverseOrder());
		System.out.println(list);
	}
}
