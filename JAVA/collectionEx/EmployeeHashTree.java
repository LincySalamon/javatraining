package com.collectionEx;

public class EmployeeHashTree {
	public int empId;
	public String empName;
	public int empSalary;
	public EmployeeHashTree(int empId, String empName, int empSalary) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empSalary = empSalary;
	}
	@Override
	public String toString() {
		return "EmployeeHashTree [empId=" + empId + ", empName=" + empName + ", empSalary=" + empSalary + "]";
	}
	
}
