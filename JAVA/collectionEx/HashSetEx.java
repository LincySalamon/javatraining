package com.collectionEx;

import java.util.HashSet;

public class HashSetEx {
	public static void main(String[] args) {
	HashSet<Object> set = new HashSet<>();
	
	set.add(Integer.valueOf(10));
	set.add(Float.valueOf(45.90f));
	set.add(23);
	set.add(23);
	set.add(27.345);
	set.add(new String("chennai"));
	set.add("chennai");
	set.add(new Integer(15));
	
	System.out.println("Set is "+set);
	}
}
