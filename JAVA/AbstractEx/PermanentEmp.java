package AbstractEx;

public class PermanentEmp extends Employee{
	
	public PermanentEmp(int empno) {
		super(empno);
		
	}

	@Override
	void doWork() {
		System.out.println("I am a permanent employee ");
	}
}
