package AbstractEx;

public abstract class Employee {
	public int empno;

	public Employee(int empno) {
		super();
		this.empno = empno;
	}
	abstract void doWork();  //abstract method
	
	public void dispDetails() //normal method
	{
		System.out.println("This is normal method ");
	}
}
