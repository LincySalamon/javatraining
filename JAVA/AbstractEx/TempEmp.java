package AbstractEx;

public class TempEmp extends Employee {
	
	public TempEmp(int empno) {
		super(empno);
		
	}

	@Override
	void doWork() {
		System.out.println("I am a temporary employee..");
	}
}
