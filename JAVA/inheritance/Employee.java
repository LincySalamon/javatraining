package inheritance;

public class Employee {
	public int empNo;
	protected String empName;
	protected float empBS;
public Employee(int empNo, String empName, float empBS) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.empBS = empBS;
	}
public float calTax()
{
	return 0;
}
}
