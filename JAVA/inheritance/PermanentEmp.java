package inheritance;

public class PermanentEmp extends Employee{
	int inSent;
	int bonus;
	
	public PermanentEmp(int empNo, String empName, float empBS, int inSent, int bonus) {
		super(empNo, empName, empBS);
		this.inSent = inSent;
		this.bonus = bonus;
	}
	public float calcTotalSal()
	{
		return empBS + inSent + bonus;
	}
	public float calTax()
	{
		float a = calcTotalSal();
		return (float) (a*0.10);
	}
	@Override
	public String toString() {
		return "PermanentEmp [inSent=" + inSent + ", bonus=" + bonus + ", empNo=" + empNo + ", empName=" + empName
				+ ", empBS=" + empBS + "]";
	}
	
}

