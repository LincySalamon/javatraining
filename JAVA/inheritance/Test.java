package inheritance;

public class Test {
	
	public static void main(String[] args) {
		PermanentEmp pem = new PermanentEmp(123,"Ajay",20000.00f,5000,10000);
		System.out.println(pem);
		System.out.println("Total Salary "+ pem.calcTotalSal());
		System.out.println("Tax is "+pem.calTax());
		
		Temporary t = new Temporary(001, "Arun", 10000, 5000, 3);
		System.out.println(t);
		System.out.println("Total Salary "+ t.calcTotalSal());
		System.out.println("Tax is "+t.calTax());
	}
}
