package inheritance;

public class Temporary extends Employee{
	int inSent;
	int ContPeriod;
	public Temporary(int empNo, String empName, float empBS, int inSent, int contPeriod) {
		super(empNo, empName, empBS);
		this.inSent = inSent;
		ContPeriod = contPeriod;
	}
	public float calcTotalSal()
	{
		return empBS+inSent;
	}
	public float calTax()
	{
		float a = calcTotalSal();
		return (float) (a*0.05);
	}
	@Override
	public String toString() {
		return "Temporary [inSent=" + inSent + ", ContPeriod=" + ContPeriod + ", empNo=" + empNo + ", empName="
				+ empName + ", empBS=" + empBS + "]";
	}
}
