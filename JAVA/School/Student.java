package School;

public class Student extends Teacher implements principal,PT_teacher  {
	String stdname;
	int rollno;
	public Student(String name, String sub, String stdname, int rollno) {
		super(name, sub);
		this.stdname = stdname;
		this.rollno = rollno;
	}
	@Override
	public void doExercise() {
		System.out.println("Just Walk the ground");
		
	}
	@Override
	public void ScoreGood() {
		System.out.println(toString()+"Score Good");
		
	}
	@Override
	public String toString() {
		return "Student [stdname=" + stdname + ", rollno=" + rollno + ", name=" + name + ", Sub=" + Sub + "]";
	}
	
}
