package com.father;

public class Father {
	public String name;	// all access
	private int cardNo; // only father
	protected String property;  // father - child
	
	public Father(String name, int cardNo, String property) {
		super();
		this.name = name;
		this.cardNo = cardNo;
		this.property = property;
	}
public static void main(String[] args) {
	Father f = new Father("name",1234,"Banglow");
}
}