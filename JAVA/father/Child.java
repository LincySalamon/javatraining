package com.father;

public class Child extends Father{
	String childName;

	public Child(String name, int cardNo, String property, String childName) {
		super(name, cardNo, property);
		this.childName = childName;
	}
	public static void main(String[] args) {
		Child c = new Child("Samuel",12345,"House","Lincy");
		System.out.println("Father name " + c.name);
		System.out.println("Child name " + c.childName);
		System.out.println("Property " + c.property);
	}
}
