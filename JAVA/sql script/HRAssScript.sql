use hr
select * from employees
select First_name,last_name from employees;
select distinct department_id from employees;

Select * From employees
Order by salary ASC;

select SUM(salary)
from employees
where salary>0;

select AVG(salary)
from employees
where salary>0;


select MIN(salary)
from employees;

select Max(salary)
from employees;

Select count(employee_id)
  From employees;

Select distinct count(job_id)
  From employees;

select upper(first_name)
  from employees;

select left (first_name,3)
 from employees;

Select 171*214+625 Result;

select ltrim(first_name) from employees;
select rtrim(first_name) from employees;
select length(first_name)  from employees;
select length(last_name)  from employees;
select * from employees Where first_name like '%[0-9]%'
select * from employees limit 10;
select  ROUND(salary,2)
  from employees;
  
use hr
select first_name,last_name,j.job_id,job_title
from employees e,jobs j 
where e.job_id = j.job_id
select first_name,last_name,e.job_id,job_title
from employees e
inner join jobs j
on e.job_id = j.job_id
select first_name,last_name,employee_id,d.department_id,department_name
from employees e, departments d
where e.department_id = d.department_id

select employee_id,first_name,last_name,e.department_id,department_name
from employees e
inner join departments d on e.department_id=d.department_id
where e.department_id between 10 and 30 
order by first_name

select employee_id,first_name,last_name,e.department_id,department_name
from employees e
inner join departments d on e.department_id=d.department_id
where e.department_id in(20,50,80)
order by first_name

select first_name,last_name ,j.job_id,job_title as "current job"  ,h.job_id,job_title as " previous job "
from employees e , jobs j , job_history  h
where e.job_id = j.job_id and e.employee_id  = h.employee_id
and j.job_id = h.job_id

select department_id,department_name,l.location_id,city
from departments d 
inner join locations l on d.location_id = l.location_id

select department_id,department_name,l.location_id,city,c.country_id,country_name
from departments d
inner join locations l on d.location_id = l.location_id
inner join countries c on l.country_id = c.country_id

select first_name,last_name,j.job_id,job_title,d.department_id,department_name
from employees e
inner join jobs j on e.job_id = j.job_id
inner join departments d on e.department_id = d.department_id

select first_name,last_name,d.department_id,department_name,city,l.location_id,country_name,c.country_id,region_name,r.region_id
from employees e
inner join departments d on e.department_id = d.department_id
inner join locations l on d.location_id = l.location_id
inner join countries c on l.country_id = c.country_id
inner join regions r on c.region_id = r.region_id

select first_name,last_name ,j.job_id,job_title as "current job"  ,h.job_id,job_title as " previous job "
from employees e , jobs j , job_history  h
where e.job_id = j.job_id and e.employee_id  = h.employee_id
and j.job_id = h.job_id

select concat(first_name,last_name) as "Manager Name" ,e.department_id,department_name 
from employees e , departments d 
where e.employee_id = d.manager_id

select first_name,last_name,d.department_id,city,l.location_id,country_name,c.country_id
from employees e,departments d, locations l, countries c 
where first_name like 'a%' and d.location_id = l.location_id and c.country_id = l.country_id

select concat(first_name,last_name)as "EmployeeName"
from employees e
inner join departments d on e.department_id = d.department_id
where d.department_name like 'sales'

select employee_id,concat(first_name,last_name) as "Employee Name",
salary from employees where salary<(select salary from employees where employee_id=116)

select hire_date from employees where first_name like "Jack"
select concat(first_name,last_name) as "Employee Name",
hire_date from employees where hire_date>(
select hire_date from employees where first_name like "Jack")

select first_name,Last_name,e.department_id,department_name
from employees e
left join departments d on d.department_id = e.department_id
select first_name,Last_name,e.department_id,department_name
from departments d 
left join employees e using(department_id)

select e.employee_id,concat(e.first_name," ",e.last_name) as "Employee Name",
e.manager_id,m.employee_id,concat(m.first_name," ",m.last_name) as "Manager Name"
from employees e , employees m
where e.manager_id = m.employee_id

select concat(first_name,last_name) as "Name",start_date,end_date
salary from employees e
inner join job_history j on e.employee_id = j.employee_id 
where salary > 10000
