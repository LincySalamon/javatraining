package com.First;

public class ConstructorDemo {
	int rollno;
	String name;
	boolean pass;
	float perc;
	char ch;
	public ConstructorDemo() {
		//user create default constructor
		rollno = 10;
		name = "ABC"; 
		pass = true;
		perc = 85.42f;
		ch = 'F';
	}

	public static void main(String[] args) {
		ConstructorDemo myobj = new ConstructorDemo();
		// object is created default constructor is created
		System.out.println("Studen Name "+ myobj.name);
		System.out.println("Roll.No "+ myobj.rollno);
		System.out.println("Gender "+ myobj.ch);
		System.out.println("Paa/fail "+ myobj.pass);
		System.out.println("Percentage "+ myobj.perc);
	}
}
