package com.First;

public class GradeCalc {
	public static void main(String[] args){
		int per=90;
		if(per >= 75)
			System.out.println("Distinction");
		else if(per > 60 && per < 74)
			System.out.println("First class");
		else if(per > 50 && per < 59)
			System.out.println("Second class");
		else if(per >35 && per < 49)
			System.out.println("Third class");
		else
			System.out.println("Fail");
	}
}
