package com.First;

public class ConstructorArgs {
	int rollno;
	String name;
	float perc;
	
	public ConstructorArgs(int no, String name, float perc) {
		super();
		rollno = no;
		this.name = name;
		this.perc = perc;
	}
	public void dispdetails() {
		System.out.println(name + "  " + rollno + "  " + perc );
		
	}
	public static void main(String[] args) {
		ConstructorArgs s1 = new ConstructorArgs(10 , "ABC" , 69.87f);
		ConstructorArgs s2 = new ConstructorArgs(20 , "XYZ" , 90.72f);
		ConstructorArgs s3 = new ConstructorArgs(30 , "LIN" , 85.9f);
		s1.dispdetails();
		s2.dispdetails();
		s3.dispdetails();
	}
}