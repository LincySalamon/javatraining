package com.First;

public class Speed {
	public static void main(String[] args) {
		double dist = 2500;	//meter
		double hr = 5; 
		double min = 56;
		double sec = 23;
		double speed1 = dist / ((hr*60*60)+(min*60)+sec);
		System.out.println("Speed in m/sec is "+ speed1);
		double speed2 = (dist/1000)/(5+(min/60)+(sec/360));
		System.out.println("Speed in km/hr is "+ speed2);
		double speed3 = (dist*0.00062137)/(5+(min/60)+(sec/360));
		System.out.println("Speed in miles/hr is "+ speed3);
	}
}
