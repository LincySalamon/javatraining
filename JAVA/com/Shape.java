package com.First;

public class Shape {
	public float area(int r) {
		float pi=3.142f;
		return pi * r * r;
	}
	public int area(int l,int b) {
		return l * b;
	}
	public float area(float b,float h) {
		return (b * h)/2;
	}
	public int area(int l, int b, int h) {
		return l * b * h;
	}
	public static void main(String[] args) {
		Shape s = new Shape();
		System.out.println("Area of circle " + s.area(7));
		System.out.println("Area of rectangle " + s.area(50, 10));
		System.out.println("Area of triangle " + s.area(2.5f, 5.0f));
		System.out.println("Surface area of cuboid " + s.area(5, 8, 2));
	}
}
