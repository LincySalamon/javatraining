package com.First;

public class ArrayExercise2 {
	public static void main(String[] args) {
		int arr[][] = new int [10][5]; //size 10
		//row = 10 ,col = 5
		for(int row = 0 ;row<10;row++)
		{
			for(int col = 0;col<5;col++)
			{
				arr[row][col] = row+col;
			}
		}
		
		for(int row = 0 ;row<10;row++)
		{
			for(int col = 0;col<5;col++)
			{
					System.out.print(arr[row][col] + "  ");
		    }
			System.out.println();
		}
}

}
