package com.First;

public class ShapePerimeter {
	public float peri(int r) {
		float pi = 3.142f;
		return 2 * pi * r;
	}
	public int peri(int l, int b) {
		return 2 * (l + b);
	}
	public float peri(float a) {
		return 4 * a;
	}
	public static void main(String[] args) {
		ShapePerimeter sp = new ShapePerimeter();
		System.out.println("Circumferance of circle is " + sp.peri(7));
		System.out.println("Perimeter of rectangle is " + sp.peri(10, 20));
		System.out.println("Perimeter of square is " + sp.peri(5.5f));
	}
}
