package com.First;

public class OverloadingEx {
	public int Add(int no, int no1) {
		return no + no1;
	}
	public int Add(int no, int no1, int no2) {
		return no + no1 + no2;
	}
	public float Add(float no, float no1, float no2) {
		return no + no1 + no2;
	}
	public static void main(String[] args) {
		OverloadingEx s = new OverloadingEx();
		System.out.println(s.Add(10,50));
		System.out.println(s.Add(10,50,30));
		System.out.println(s.Add(10.9f,5.4f,3.89f));
	}
}
