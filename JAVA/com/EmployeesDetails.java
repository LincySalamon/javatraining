package com.First;

public class EmployeesDetails {
	public int empNo;
	public String empName;
	public float empSalary;
	
	public EmployeesDetails(int empNo, String empName, float empSalary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.empSalary = empSalary;
	}
	public static void main(String[] args) {
		EmployeesDetails obj1 = new EmployeesDetails(123,"Arun",30000f);
		EmployeesDetails obj2 = new EmployeesDetails(342,"Arjun",30500.00f);
		EmployeesDetails obj3 = new EmployeesDetails(567,"Venkat",25000.856f);
		EmployeesDetails obj4 = new EmployeesDetails(123,"Raja",68000f);
		EmployeesDetails obj5 = new EmployeesDetails(123,"Hari",10000f);
		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println(obj3);
		System.out.println(obj4);
		System.out.println(obj5);
	}
	@Override
	public String toString() {
		return "EmployeesDetails [empNo=" + empNo + ", empName=" + empName + ", empSalary=" + empSalary + "]";
	}
}
