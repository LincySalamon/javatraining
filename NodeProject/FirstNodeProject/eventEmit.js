//importing the events
const EventEmitter = require('events');

//inintialize the event
var eventEmitter =new EventEmitter();

//registering the MyEvent
/*eventEmitter.on('MyEvent',(msg) =>
{
    console.log(msg);
});
//trigger the event*/


const fun1 = (msg) =>
{
    console.log(msg);
};
const fun2 = (msg) =>
{
    console.log("from fun2" + msg);
};
eventEmitter.on('MyEvent',fun1);
eventEmitter.on('MyEvent',fun2); //calling more than one method using event
eventEmitter.emit('MyEvent',"This is my first event created");
//eventEmitter.removeAllListeners;