var express = require('express');
var app = express();

app.get('/', function (req, res) {
   res.send('Hello World');
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})

/*
var http = require('http');//import node.js coremodule
var server = http.createServer(function(req,res) {//create web server
    if(req.url == '/'){ // check the URL os the current req
        //set response header
        res.writeHead(200,{'Content-Type': 'text/html'});
        //set response content
        res.write('<html><body><p>This is Home Page</p></body></html>');
        res.end();
    }
    else if(req.url == "/student"){
        res.writeHead(200,{'Content-Type': 'text/html'});
        res.write('<html><body><p>This is Student page</p></body></html>');
        res.end();
    }
    else if(req.url == "/admin"){
        res.writeHead(200,{'Content-Type': 'text/html'});
        res.write('<html><body><p>This is Admin Page</p></body></html>');
        res.end();
    }
    else{
        res.end('Invalid Request!!');
    }
});
server.listen(3000);//listen for any incoming requests*/
console.log('Node.js web server at port 3000 is running...');