const EventEmitter = require('events');

//inintialize the event
var eventEmitter =new EventEmitter();

const fun1 = (msg) =>
{
    console.log(msg);
};
const fun2 = (msg) =>
{
    console.log("from fun2" + msg);
};
eventEmitter.on('error',(err) =>
    console.log("Oh! Something went wrong"));
eventEmitter.on('newListener',(event,Listener) =>
    console.log(`new Listener added it this ${event}`));
eventEmitter.on('remoeListener',(event,Listener) =>
    console.log(`new Listener removed from this ${event}`));
//add
eventEmitter.on('MyEvent',fun1);
eventEmitter.on('MyEvent',fun2);
//remove listener
eventEmitter.off('MyEvent',fun1);
//trigger the event
eventEmitter.emit('MyEvent',"This is my first event created");
eventEmitter.emit('error',new Error("OOOOPPssss"));