console.log("This is my First node Project");
var a = 10;
let b =2;
const c =3;
console.log(a);
var a = 5; // allowed
console.log(a);
console.log(b);
//let b = 10; // show's an error
console.log(b);

const print = (msg) =>
{
    console.log(msg);
};

print("This is from method");
print("Again calling a method");
const http = require('http');
http.createServer(function(request,response){
    console.log("server started");
    response.write("Node server created and running");
    response.end();
}).listen(3000);

const express = require("express");
const app = express();
app.get("/",(req,res)=>{
   res.send("Node with express is up and running"); 
});
app.listen(3000,()=> 
 {
     console.log("Node with express is up and running and this is console");
 });
