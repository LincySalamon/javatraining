let colors = ["Blue","Green","Red","Purple"];
//console.log("Before Loop");
const getcolors = () =>
{
    setTimeout(() =>{
        colors.forEach(data => 
        {
            console.log(data);
        });
    },5000);
}
const postcolors = (color,getcolors) =>
{
    return new Promise((resolve,reject)=>
    {
        setTimeout(() =>
        {
            let error = true;
            if(!error)
            {
            colors.push(color); resolve();
            }
            else{
                reject("Something went wrong");
            }
        },2000);
    });
}
/*postcolors("White")
.then(getcolors)
.catch(error => {
    console.log(error);
});*/

const init = async() =>
{
    try{
        await postcolors("Black");
        getcolors();
    }
    catch(error)
    {
        console.log(error);
    }
}
init();