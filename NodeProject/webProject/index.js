var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var path = require('path');
var session = require('express-session');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));
app.use(session({
    secret : "secret",
    resave : true,
    saeUninitialize : true
}));
//give the connection settings
var conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "Root123*",
    //port
    database: "webProject"
});

//establish connection 

conn.connect(function(err)
{
    if (err) throw err;
    console.log("Connected to mysql sucessfully");
});
    //show a html page
app.get("/",function(request,response)
{
    response.sendFile(path.join(__dirname + '/frontpage.html')); // remember always to add a relative path 
});
app.post("/gologin",function(requst,response)
{
    response.sendFile(path.join(__dirname + '/login.html'));
});
app.post("/auth",function(request,response) // execute when html -submit is pressed 
{
    var username = request.body.username;
    var password = request.body.password;
    console.log(username + "  " + password);
   // check in database
   conn.query("select * from accounts where username = ? and password = ?",[username,password],function(error,result)
   {
  if(error) throw error;
    if(result.length >0)
    {
        console.log("Login sucess"); 
        //start the session 
        request.session.loggedin = true; // unique sessionId created 
        //carry forward 
        request.session.username  = username;
        response.redirect('/home');
        response.end();
    }
    else 
    console.log("Login is not success");
});
});

app.get("/home",function(request,response)
{
if(request.session.loggedin)
{
   response.send("Welcome to your Mail box " + request.session.username);
   request.session.destroy();// with log out
}
else 
{
 response.send("Login First !!!!  " );
}
response.end();
});

app.listen(3000); 
