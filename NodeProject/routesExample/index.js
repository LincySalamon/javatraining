const express = require("express");
const app =express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());    //sets midlleware mandatory
app.use(bodyParser.urlencoded({
    extended:true,
})
);
app.get("/",(_req,res) =>
{
    res.send("Node with express is up and running");
});
app.listen(3000,() =>
{
    console.log("Node with express is up and running and this is console");
});
const routes = require("./routes/routes");
app.use("/routes",routes);
